(+ 1 2)
(if t 1 2)
(if nil 1 2)

(my-last '(1 2 3 4))
(if t
    (+ 1 2)
    (* 3 4))

(defun my-but-last (x)
  (if (cddr x)
      (my-but-last (cdr x))
      x))

(f x y z)

(my-but-last '(1 2))

(my-but-last '(1 2 3 4))

(defun element-at (l n)
  (if (= 1 n)
      (car l)
      (element-at (cdr l) (- n 1))))

(element-at '(1 2 3) 3)

(defun my-length (l)
  (if l
      (+ 1 (my-length (cdr l)))
      0))

(my-length '(1 2 3 4))

(defun my-flatten (x)
  (if (atom x)
      (list x)
      (if (null (cdr x))
          (my-flatten (car x))
          (append (my-flatten (car x)) (my-flatten (cdr x))))))

(defun my)

(my-flatten '(a (b (c d) e)))

(my-flatten '(a b c d e))


(defun compress (xs)
  (labels ((iter (prev xs acc)
      (if (null xs)
   (reverse acc)
   (let ((rest (cdr xs)))
     (if (equal prev (car xs))
         (iter prev rest acc)
         (iter (car xs) rest (cons (car xs) acc)))))))
    (if (null xs)
 xs
 (iter (car xs) (cdr xs) (list (car xs))))))

(compress '(a a a a b c c a a d e e e e))

;; 9
(defun pack (xs)
  (labels ((iter (prev-acc xs ret)
      (if (null xs)
   (reverse (cons prev-acc ret))
   (if (null prev-acc)
       (iter (list (car xs)) (cdr xs) ret)
       (if (equal (car prev-acc) (car xs))
    (iter (cons (car xs) prev-acc) (cdr xs) ret)
    (iter (list (car xs)) (cdr xs) (cons prev-acc ret)))))))
    (if (null xs)
 nil
 (iter nil xs nil))))

(pack '())

(pack '(a a a a b c c a a d e e e e))

;; 10
(defun encode (xs)
  (mapcar #'(lambda (x) (list (car x) (length x))) (pack xs)))

(encode '(a a a a b c c a a d e e e e))

;; 11
(defun encode-modified (xs)
  (mapcar #'(lambda (x) (if (= (length x) 1)
       (car x)
       (list (car x) (length x))))
   (pack xs)))

(encode-modified '(a a a a b c c a a d e e e e))

(defun repeat (x n)
  (if (= n 0)
      nil
      (cons x (repeat x (- n 1)))))

;; 12
(defun decode (xs)
  (mapcan #'(lambda (x)
       (if (atom x)
    (list x)
    (repeat (car x) (cadr x))))
   xs))

(decode (encode-modified '(a a a a b c c a a d e e e e)))

;; 13
(defun encode-direct (xs)
  (labels ((iter (prev acc xs ret)
      (if (null xs)
   (reverse (cons (if (= 1 acc)
        prev
        (list prev acc))
    ret))
   (if (equal prev (car xs))
       (iter prev (1+ acc) (cdr xs) ret)
       (iter (car xs) 1 (cdr xs) (cons (if (= 1 acc)
        prev
        (list prev acc))
           ret))))))
    (if (null xs)
 nil
 (iter (car xs) 1 (cdr xs) nil))))

(encode-direct '(a a a a b c c a a d e e e e))

;; 14


(a a a a b c c a a <=  d e e e e)

;; acc: (a)
;; xs: (a d e e e e)
;; ret: ((a a a a) (b) (c c)) => ((c c) (b) (a a a a))

;; =>

;; acc: (a a)
;; xs: (d e e e e)
;; ret:

(defun my-reverse-iter
    (xs acc))

(defun my-reverse
    (xs)
  (my-reverse-iter xs nil))

;; design recipe for tree

(defun my-tree-function (tree)
  (if (null tree)
      ;; treeが空木のときの答え
      (if (atom tree)
          ;; treeが葉のときの答え
          (let ((car-ans (my-tree-function (car tree)))
                (cdr-ans (my-tree-function (cdr tree))))
            ;; treeがノードのときの答え. car-ans, cdr-ansを組み合わせたりしてもよい(不要なこともある)
            ))))

(= (leaf-count '()) 0)

(= (leaf-count '1) 1)

(= (leaf-count '(1 (3 2))) 3)

(= (leaf-count '(1 ((2 3) 4) 5) 6 7) 7)

(defun leaf-count (tree)
  (if (null tree)
      ;; treeが空木のときの答え
      (if (atom tree)
          ;; treeが葉のときの答え
          (let ((car-ans (leaf-count (car tree)))
                (cdr-ans (leaf-count (cdr tree))))
            ;; treeがノードのときの答え. car-ans, cdr-ansを組み合わせたりしてもよい(不要なこともある)
            ))))

(defun leaf-count (tree)
  (if (null tree)
      0
      (if (atom tree)
          1
          (let ((car-ans (leaf-count (car tree)))
                (cdr-ans (leaf-count (cdr tree))))
            (+ car-ans cdr-ans)))))

(defun tree-sum (tree)
  (if (null tree)
      0
      (if (atom tree)
          tree
          (let ((car-ans (tree-sum (car tree)))
                (cdr-ans (tree-sum (cdr tree))))
            (+ car-ans cdr-ans)))))

(= (tree-sum '()) 0)

(= (tree-sum '2) 2)

(= (tree-sum '(1 (3 2))) 6)

(= (tree-sum '(1 ((2 3) 4) 5 6 7)) 28)

(leaf-count '(1 ((2 3) 4) 5 6 7))

(defun my-flatten
    (tree)
  (if (null tree)
      tree
      (if (atom tree)
          (list tree)
          (let ((car-ans (my-flatten (car tree)))
                (cdr-ans (my-flatten (cdr tree))))
            (append car-ans cdr-ans)))))

(my-flatten '(1 ((2 3) 4) 5 6 7))

;; 26
(defun combination (n es)
  (if (or (= n 0) (< n (length es)))
      '(())
      (let ((rest (cdr es))
            (res1 (mapcar #'(lambda (c) (cons (car es) c)) (combination (- n 1) (cdr es))))
            (res2 (combination n (cdr es))))
        (append res1 res2))))

(combination 4 '(a b c d))

(combination 0 '(a b c d))
(mapcar #'(lambda (c) (cons 'a c)) (combination 0 '(b c d)))

;; design recipe for list

;; 型は list X -> Y
(defun list-func (l)
  (if (null? l)
      if-null-ans
      (let ((ans-for-cdr (list-func (cdr l))))
	if-not-null-ans ;; ここで、ans-for-cdrだったり、(car l)を使ったりする。
	)))

;; listの長さを測る関数をデザインレシピで書いてみる。
;; 型は list X -> int

;; if-null-ans = (my-length '()) = 0
;; if-not-null-ans = 1 + ans-for-cdr
(defun my-length (l)
  (if (null l)
      0
      (let ((ans-for-cdr (my-length (cdr l))))
	(+ 1 ans-for-cdr))))

;; reverseをデザインレシピで書いてみる。
;; 型は list X -> list X
;; if-null-ans = (my-reverse '()) = ()
;; if-not-null-ans = ???
;; 具体的に値を入れて考えてみる
;; (my-reverse '(a b c d)) を計算したい
;; (my-reverse '(b c d)) = (d c b) はすでにわかっている
;; (my-reverse '(a b c d)) = '(d c b a)
;; carをans-for-cdrの最後に付けてやればいよい
;; appendを使う
(defun my-reverse (l)
  (if (null l)
      '()
      (let ((ans-for-cdr (my-reverse (cdr l))))
	(append ans-for-cdr (list (car l))))))

(my-reverse '(a b c d))

;; このデザインレシピを使うと、関数が末尾再帰的にならないことに注意
;; 末尾再帰をするためには、引数を1つ追加してやる。
;; l : list X
;; acc : Y
;; list-iter-func : list X -> Y -> Y
(defun list-iter-func (l acc)
  (if (null l)
      acc ;; ここでは、accをそのまま返しているが、accがリストなら(reverse acc)などとすることも多い
      (let ((next-acc (do-something-with acc (car l))))
	(list-iter-func (cdr l) next-acc))))

;; accの初期値の決め方
;; (list-func l) = (list-func-iter l init-acc) として使うことが多い。
;; (list-func '()) = init-accとなるように、init-accを決める必要がある。

;; 末尾再帰用のデザインレシピでlengthを書き直してみる
;; acc: int
(defun my-length-iter (l acc)
  (if (null l)
      acc
      (let ((next-acc (+ acc 1)))
	(my-length-iter (cdr l) next-acc))))

;; init-acc: (length ()) = 0 なので、0.
(defun my-length2 (l)
  (my-length-iter l 0))

(my-length2 '(a b c))

;; reverseを末尾再帰のデザインレシピで書いてみる
(defun my-reverse-iter (l acc)
  (if (null l)
      acc
      (let ((next-acc (cons (car l) acc)))
	(my-reverse-iter (cdr l) next-acc))))

;; init-acc: (reverse ()) = () なので、().
(defun my-reverse2 (l)
  (my-reverse-iter l '()))

;; my-reverseの場合は、末尾再帰のデザインレシピのほうがappendを使わないので簡単と言えば簡単。
;; たまに末尾再帰版のほうが書きやすいことがあるので、普通のデザインレシピでうまく行かないときは
;; 末尾再帰で考えたほうが良いかも。

;; P08: compressを末尾再帰で書いてみる。逆に、末尾再帰以外では書きづらい気がする。
(defun compress-iter (l acc)
  (if (null l)
      (reverse acc) ;; 最後にreverseするpattern
      (let ((next-acc (if (null acc)
			  (cons (car l) acc)
			  (if (eq (car l) (car acc))
			      acc
			      (cons (car l) acc)))))
	(compress-iter (cdr l) next-acc))))

(defun compress2 (l)
  (compress-iter l ()))

(compress2 '(a a a a b c c a a d e e e e))


;; p09とかもこのパターンでできるのでやってみよう
