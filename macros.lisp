(defmacro nil! (var)
  (list 'setq var nil))
(defmacro backquote-nil! (var)
  `(setq ,var nil))

(setq a 1 b 2 c 3)

'(a b c)
`(a ,b c)

`(a (,b c))

`(a b ,c (',(+ a b c)) (+ a b) 'c '((,a ,b)))

(setq x 10)

`(setq x nil)

(nil! x)

(defmacro nif (expr pos zero neg)
  `(case (truncate (signum ,expr))
     (1 ,pos)
     (0 ,zero)
     (-1 ,neg)))

(defmacro nif (expr pos zero neg)
  (list 'case
        (list 'truncate (list 'signum expr))
        (list 1 pos)
        (list 0 zero)
        (list -1 neg)))

 (mapcar #'(lambda (x)
              (nif x 'p 'z 'n))
          '(0 2.5 -8))

(setq b '(1 2 3))

(1 1 2 3 3)
`(1 ,b 3)
`(1 ,@b 3)

(defmacro our-when (test &body body)
  `(if ,test
       (progn
         ,@body)))

(our-when t 1 2 3)

;; difference between &body and &rest
;; -> https://groups.google.com/forum/#!topic/comp.lang.lisp/tMgTi0Z_TzM
;; &body gives smart editors and code formatters a hint about how to
;; indent the code.  apart from that, they do exactly the same thing.

;; 7.3

;; note: eq vs eql
;; https://stackoverflow.com/questions/547436/whats-the-difference-between-eq-eql-equal-and-equalp-in-common-lisp
;; (eq x y) is true if and only if x and y are the same identical object.
;; The eql predicate is true if its arguments are eq, or if they are numbers of the same type with the same value, or if they are character objects that represent the same character.


(defmacro memq (obj lst)
  `(member ,obj ,lst :test #'eq))

(defmacro while (test &body body)
  `(do ()
       ((not ,test)) ;; doは終了条件を書くのでwhileを書きたいなら継続条件に変換しないといけない...
     ,@body))

(pprint (macroexpand '(while (while (able) nil) (laugh)))) ;; doまで展開しちゃう
(pprint (macroexpand-1 '(while (able) (laugh))))
;; clojureで言うところのmacroexpand, macroexpand-1, macroexpand-allはないようだ
;; macroexpand (cl) = macroexpand-all (clj) ???

;; debuggin aid
(defmacro mac (expr)
  `(pprint (macroexpand-1 ',expr)))

(mac (or x y))
(mac (or x y z))

(setq myexp (macroexpand-1 '(memq 'a '(a b c))))
(eval myexp)

(destructuring-bind (x (y) . z) '(a (b) c d)
  (list x y z))

(let ((x 100))
  (dolist (x '(a b c) (list x))
    (print x)))

(mac (dolist (y '(a b c) y)
       (print y)
       (print y))) ;; ???

;; mapcはリストが返される(副作用を期待している

(defmacro our-dolist ((var list &optional result) &body body)
  `(progn
     (mapc #'(lambda (,var) ,@body)
           ,list)
     (let ((,var nil)) ;; (body内に現れるvarはキャプチャされないのにresultにあるvarがキャプチャされるのは直感的でないからこうしている？？？
       ,result)))

(mac (our-dolist (x '(a b c))
       (print x)
       (print x)))

;;最後のletは多分gensymがどうとかいう脚注の話だろうがよくわからん
;; どういうケースで問題になるか作れなかった

;; (defmacro bad-our-dolist ((var list result) &body body) ...)
;; 訳注の「オプショナル引数と本体の先頭との区別がつかない」は「本体の末尾」の間違いか？

(mac (our-dolist (x '(a b c))
       (print x)))

(our-dolist (x '(a b c))
  (print x))

(let ((x 1))
  (our-dolist (x '(a b c) 21) ;; result is 21
    (print x)))

(defmacro when-bind ((var expr) &body body)
  `(let ((,var ,expr))
     (when ,var
       ,@body)))

(mac (when-bind (input (get-user-input))
       (process input)))

;; defmacro is macro
(mac (defmacro when-bind ((var expr) &body body)
       `(let ((,var ,expr))
	  (when ,var
	    ,@body))))

;; &whole wakaran (ググったけど見つからないので面倒くさいので諦め）

;; our-defmacro

(defmacro our-expander (name) `(get ,name 'expander))

(defmacro our-defmacro (name parms &body body)
  (let ((g (gensym)))
    `(progn
       (setf (our-expander ',name)
             #'(lambda (,g) ;; (macro-name arg1 arg2 arg3)
                 (block ,name
                        (destructuring-bind ,parms (cdr ,g) ;; (cdr ,g) is macro args
                          ,@body))))
       ',name)))

;; block is nani
;; https://www.cs.cmu.edu/Groups/AI/html/cltl/clm/node85.html
;; C言語のラベルみたいな感じか。ここでわざわざ使っているのはよくわからないけど。。。
;; The block construct executes each form from left to right, returning whatever is returned by the last form
;; (return-from name result ...) で脱出してresult...を返すっぽい（多値？）

(defun our-macroexpand-1 (expr) ;; (macro-name arg1 arg2 arg3)
  (if (and (consp expr) (our-expander (car expr)))
      (funcall (our-expander (car expr)) expr)
      expr))

;; 「例えば、本書で定義したマクロはいずれも正しく機能する」を確かめてみる
(mac (our-defmacro nil! (x) `(setq ,x nil)))
(our-expander 'nil!)
(our-macroexpand-1 '(nil! x))
(setq x 10)
(eval (our-macroexpand-1 '(nil! x)))

;; また本物のdefmacroが第一引数のmacro-functionとして保持させる関数は引数を２個取る
;; マクロ呼び出し全体（マクロ呼び出しの式のことかな）と、
;; それを含むレキシカル環境だ
(macro-function 'our-defmacro)


(setq setter 'nil)
(let ((op 'setq))
;;  (setq setter (lambda () (setf op 'xxx)))
  (defmacro our-setq (var val)
    (list op var val)))

(our-setq y 10) ;; works
y


(funcall setter)


(setq op 'setq)
(defmacro our-setq (var val)
  (list op var val))
(mac (our-setq x 10))
(setq op 'xxxxxxxxxxxx)
(mac (our-setq x 10))


;; CLTL2準拠の？処理系のdefmacroの上に乗っていればour-defmacroも動く
(let ((op 'setq))
  (our-defmacro our-setq (var val)
    (list op var val)))

(eval (our-macroexpand-1 '(our-setq y 10)))

;; experiment
op
(defmacro our-setq (var val)
  (list op var val))
(macroexpand-1 '(our-setq z 10)) ;; variable op not bound

;; 7.7
;; マクロour-doを書いてみよう

 (let ((a 1))
    (setq a 2 b a)
    (list a b))

;; psetq
(let ((a 1))
  (psetq a 2 b a)
  (list a b))

(do ((w 3)
     (x 1 (1+ x))
     (y 2 (1+ y))
     (z)) ;; z
  ((> x 10) (princ z) y) ;; 終了条件
  (princ x)
  (princ y))
;; =>
(prog ((w 3) (x 1) (y 2) (z nil)) ;; 変数束縛
      foo
      (if (> x 10)
          (return (progn (princ z) y)))
      (princ x)
      (princ y)
      (psetq x (1+ x) y (1+ y))
      (go foo))

'(our-do bindforms (test result ...) body ...)

(defmacro our-do (bindforms (test &rest result) &body body)
  (let ((label (gensym)))
    `(prog ,(make-initforms bindforms)
           ,label ;; our-do-label
           (if ,test
               (return (progn ,@result)))
           ,@body
           (psetq ,@(make-stepforms bindforms))
           (go ,label))))

(our-do ((w 3)
	 (x 1 (1+ x))
	 (y 2 (1+ y))
	 (z))
	((> x 10) (princ z) y)
	(princ x)
	(princ y))

(setq bindforms-example '((w 3) (x 1 (1+ x)) (y 2 (1+ y)) (z)))
(setq test-example '(> x 10))
(setq result-example '((princ z) y))
(setq body-example '((princ x) (princ y)))

'((w 3) (x 1 (1+ x)) (y 2 (1+ y)) (z))
;; =>
'((w 3) (x 1) (y 2) (z nil))
(consp '(z))
(car '(z))
(cadr '(z))
(car (cdr '(z)))
(defun make-initforms (bindforms)
  (mapcar #'(lambda (b)
	      (princ b)
              (if (consp b)
                  (list (car b) (cadr b))
                  (list b nil)))
          bindforms))

(make-initforms bindforms-example)

(defun make-stepforms (bindforms)
  (mapcan #'(lambda (b)
              (if (and (consp b) (third b))
                  (list (car b) (third b))
                  nil))
          bindforms))

(make-stepforms bindforms-example)

;; real do macro

(mac (do ((w 3)
	  (x 1 (1+ x))
	  (y 2 (1+ y))
	  (z))
	 ((> x 10) (princ z) y)
       (princ x)
       (princ y)))

;;(mac (progn 1 2 3))
;; tagbodyってなんだ
;; 雰囲気わかるような...
;; http://clhs.lisp.se/Body/s_tagbod.htm

;; そろそろletがかけるようになってきたので時間を取ってやってみてもいいかもしれない

;; 7.8 マクロのスタイル
;; expander-code = definition of macro
;; expanded-code = そのままの意味

(defmacro our-and (&rest args)
  (case (length args) ;;
    (0 t)
    (1 (car args))
    (t `(if ,(car args)
            (our-and ,@(cdr args))))))

(mac (our-and 1 2 3))

(defmacro our-andb (&rest args)
  (if (null args)
      t
      (labels ((expander (rest)
                         (if (cdr rest)
                             `(if ,(car rest)
                                  ,(expander (cdr rest)))
                             (car rest))))
        (expander args))))
(mac (our-andb 1 2 3))

;; コンパイルされたら違いがないってこと？

;; 7.9
(defmacro magro (x) `(1+ ,x))

(setq fn (compile nil `(lambda (y) (magro y))))

(defmacro magro (x) `(+ ,x 100))

(funcall fn 1)

;; 7.10 関数からマクロへ

;; 2. パラメータリストにパラメータの名前のみが含まれ　=> &restみたいなキーワードがないってことですかね

;; 呼び出し側の関数が新たなローカルな束縛を生み出すような環境では使うべきではない　どういうことかわからない

(defmacro sum (&rest args)
  `(apply #'+ (list ,@args)))

(defmacro sum (&rest args)
  `(+ ,@args))

(mac (sum 1 2 3))

(defmacro bad-sum (&rest args)
  `(apply #'+ ,args))
(mac (bad-sum 1 2 3))

(defmacro sum (&rest args)
  `(apply #'+ ,@args))
(mac (sum 1 2 3))

;;; 条件７については　のところがわからない

;; 7.11
(symbol-macrolet ((hi (progn (print "Howdy")
			     1)))
  (+ hi hi hi))
