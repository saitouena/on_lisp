(defun joiner (obj)
  (typecase obj
    (cons #'append)
    (number #'+)))

(typecase (cons 1 nil)
  (cons 'cons)
  (number 'number))

;; 「直交」というのは多分線形代数の基底のイメージから取ってそう

(defun join (&rest args)
  (apply (joiner (car args)) args))

(join 1 2 3 4 5)
(join '(1 2 3 4) '(5 6 7))

(let ((n 10))
  (funcall add3 2)) ;;

;; package lock err
(defun complement (fn)
  #'(lambda (&rest args) (not (apply fn args))))

#'complement
(remove-if (complement #'oddp) '(1 2 3 4 5 6))
(remove-if #'evenp '(1 2 3 4 5 6))

(setf (get 'ball 'color) 'red)

(setf (cons 1 2) 1)

(setf (cdr (cons 1 2)) 1)
(setq x (cons 1 2))
(setf (cdr x) 1)
x

;; 5.2
;; TODO: setfを理解する

(defvar *!equivs* (make-hash-table))

(defun ! (fn)
  (or (gethash fn *!equivs*) fn)) ;; 見つからなければfnを返すのは良くない気がするが

(defun def! (fn fn!)
  (setf (gethash fn *!equivs*) fn!))

(def! #'remove-if #'delete-if)
(! #'remove-if)

;; 5.3
(defun memoize (fn)
  (let ((cache (make-hash-table :test #'equal)))
    #'(lambda (&rest args)
	(multiple-value-bind (val win) (gethash args cache)
	  (if win
	      val
	      (setf (gethash args cache)
		    (apply fn args)))))))

(f 0)
(f 0 1)

(setf slowid (memoize #'(lambda (x) (sleep 5) x)))
slowid
(funcall slowid 10)
(funcall slowid 10)
(funcall slowid 1)
(equal :aaa :aaa)

;; 「キーワードパラメータをとるとき...」???

;; 5.4

(defun compose (&rest fns)
  (if fns
      (let ((fn1 (car (last fns)))
            (fns (butlast fns)))
        #'(lambda (&rest args)
                   (reduce #'funcall fns
                           :from-end t ;; from-endは右結合か左結合かを決める。tなら右結合 (1 + (2 + 3)) or ((1+2)+3)
                           :initial-value (apply fn1 args))))
      #'identity))
(lambda (. args)
  (reduce (lambda (f v) (f v)) (apply fn args) fns))
;; (compose fn3 fn2 fn1)
;; (funcall #'fn3 (funcall #'fn2 (apply fn1 args)))

(butlast '(1 2 3 4))
;; => (1 2 3)
(last '(1 2 3 4))
;; => (4)

(funcall (compose #'1+ #'find-if) #'oddp '(2 3 4))

(1+ (find-if #'oddp '(2 3 4)))

;; package lock error
(defun complement (pred)
  (compose #'not pred))

(mapcar #'(lambda (x)
            (if (slave x)
                (owner x)
                (employer x)))
        people)

(defun fif (if then &optional else)
  #'(lambda (x)
      (if (funcall if x)
          (funcall then x)
          (if else (funcall else x)))))

(defun fint (fn &rest fns) ;; int = intersection (fint p q r) == (lambda (x) (and (p x) (q x) (r x)))
  (if (null fns)
      fn
      (let ((chain (apply #'fint fns)))
        #'(lambda (x)
            (and (funcall fn x) (funcall chain x))))))

(defun fun (fn &rest fns) ;; un = union
  (if (null fns)
      fn
      (let ((chain (apply #'fun fns)))
        #'(lambda (x)
            (or (funcall fn x) (funcall chain x))))))

;; 5.5

(defun our-length (lst)
  (if (null lst)
      0
      (1+ (our-length (cdr lst)))))

(defun our-every (fn lst)
  (if (null lst)
      t
      (and (funcall fn (car lst))
           (our-every fn (cdr lst)))))

(defun lrec (rec &optional base)
  (labels ((self (lst)
                 (if (null lst)
                     (if (functionp base)
                         (funcall base)
                         base)
                     (funcall rec (car lst)
                              #'(lambda () ;; cdr部の結果を生成する「関数」
                                  (self (cdr lst)))))))
    #'self))

;; length
(funcall (lrec #'(lambda (x f) (1+ (funcall f))) 0)
	 '(1 2 3 4 5))

;; every-oddp
(funcall (lrec #'(lambda (x f) (and (oddp x) (funcall f))) t)
	 '(1 2 3 5))

;; 再帰途中に関数recに与えられる引数は， その時点でのリストのcdr部と再帰呼び出しを形成する関数の2個だ．
;; 誤植っぽい. cdr -> car のはず？
;; 読み方: (funcall f) がcdr部に対する再帰の「結果」をかえすと見るといい.

;; our-every等の再帰呼び出しを行う部分が最後に来る関数には， 第1引数が偽を返したその時点で止まって欲しい． これは再帰呼び出しに渡された引数は値でなく， 必要があれば呼び出して値を求められる関数でなければならないということだ．

;; bad-lrecを考えてみる

(defun bad-lrec (rec &optional base)
  (labels ((self (lst)
                 (if (null lst)
                     (if (functionp base)
                         (funcall base)
                         base)
                     (funcall rec (car lst)
			      ;; ここでselfを呼び出して再帰する
                              (self (cdr lst))))))
    #'self))

(funcall (bad-lrec #'(lambda (x res) (1+ res)) 0)
	 '(1 2 3 4 5))
(princ '(1 2 3 4 5))
(funcall (bad-lrec #'(lambda (x res) (princ x) (and (oddp x) res)))
	 '(1 2 3 4 5)) ;; print 54321
(funcall (lrec #'(lambda (x res) (princ x) (and (oddp x) res)))
	 '(1 2 3 4 5))
;; 2を見た時点で評価をやめてほしいが全部見ているのでだめ

;; copy-list
(funcall (lrec #'(lambda (x f) (cons x (funcall f))))
	 '(1 2 3 4 5))
; remove-duplicates
(funcall (lrec #'(lambda (x f) (adjoin x (funcall f))))
	 '(1 1 1 3 1 3))
; find-if, for some function fn
(lrec #'(lambda (x f) (if (fn x) x (funcall f))))
; some, for some function fn
(lrec #'(lambda (x f) (or (fn x) (funcall f))))

;; 5.6

;; (1 . 2) の.を節点とみなすと２分木にみえてくる

(setq x '(a b)
      listx (list x 1))

(eq x (car (our-copy-list listx)))

(eq x (car (copy-tree listx)))

(defun our-copy-list (lst)
  (if lst
      (cons (car lst) (our-copy-list (cdr lst)))
      nil))


(defun our-copy-tree (tree)
  (if (atom tree)
      tree
      (cons (our-copy-tree (car tree)) ;; ここのconsがあるので一つ上の式はNILになる
            (if (cdr tree) (our-copy-tree (cdr tree))))))

(defun count-leaves (tree)
  (if (atom tree)
      1
      (+ (count-leaves (car tree))
         (or (if (cdr tree) (count-leaves (cdr tree)))
             1)))) ;; ここを0にすれば直感的になる

(count-leaves '((a b (c d)) (e) f))

;; 隠れたnil ((a b (c d nil) nil) (e nil) f nil)

;; mklistはsbclにはないので注意
(defun mklist (obj)
  (if (listp obj) obj (list obj)))

(defun flatten (tree)
  (if (atom tree)
      (mklist tree)
      (nconc (flatten (car tree)) ;; nconcはだいたいappend
             (if (cdr tree) (flatten (cdr tree))))))

(defun rfind-if (fn tree)
  (if (atom tree)
      (and (funcall fn tree) tree)
      (or (rfind-if fn (car tree))
          (if (cdr tree) (rfind-if fn (cdr tree))))))

;; addp -> oddp
(rfind-if (fint #'numberp #'oddp) '(2 (3 4) 5))

;; tree traversal
(defun ttrav (rec &optional (base #'identity))
  (labels ((self (tree)
                 (if (atom tree)
                     (if (functionp base)
                         (funcall base tree)
                         base)
                     (funcall rec (self (car tree))
                              (if (cdr tree)
                                  (self (cdr tree)))))))
    #'self))
;; こっちのほうが普通に２つ結果(lrecだと１つは結果を計算する関数だった)を受け取る関数をかけば良いのでわかりやすいかも

;; copy-tree
(ttrav #'cons #'identity)


;; 平坦リストに対する再帰では基底の値は必ずnilだが
;; lengthとかではnilでなくて0な気がするが...

;; count leaves
(ttrav #'(lambda (l r) (+ l (or r 1))) 1)

;; flatten
(ttrav #'nconc #'mklist)

(defun trec (rec &optional (base #'identity))
  (labels
    ((self (tree)
           (if (atom tree)
               (if (functionp base)
                   (funcall base tree)
                   base)
               (funcall rec tree
                        #'(lambda () ;; 関数を引数で渡された関数に渡すので、呼び出し側で制御できる
                            (self (car tree)))
                        #'(lambda ()
                            (if (cdr tree)
                                (self (cdr tree))))))))
    #'self))

(trec #'(lambda (o l r) (nconc (funcall l) (funcall r)))
      #'mklist)

(trec #'(lambda (o l r) (or (funcall l) (funcall r)))
      #'(lambda (tree) (and (oddp tree) tree)))

;; oを使う例がわからない...
;; すべての部分treeを返す関数 all-subtree
(funcall (trec #'(lambda (o l r) (append (list o)
					 (funcall l)
					 (funcall r)))
	       #'(lambda (tree) (list tree)))
	 '(2 (3 4) 5)) ;; (4) と 4 が入っててキモいけどだいたいこんな感じな気がする

;; 5.7
;; 「それをつかってもよいことはない」 => 本だと「それは間に合わないことになるかもしれない」

#.(compose #'oddp #'truncate)

;; 関数呼び出しも評価できるのすごい気がする
;; http://www.lispworks.com/documentation/HyperSpec/Body/02_dhf.htm
;; リーダマクロの気持ちがわからない、さすがに評価に制限がありそうな気がするが...
;; なんでも評価できるわけはなさそう
;; リーダマクロの章(17)があるのでそこでわかるのでは

(defun f (g x)
  #'(lambda (funcall g h x)))
