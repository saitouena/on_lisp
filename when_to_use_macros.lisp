(setq x 10)

(defun setq-fun (y)
  (setq y 3))

(setq-fun 'x)


(defun return-f ()
  (progn
    1
    (return-from return-f 2)
    3))

(return-f)

(lambda (x) (progn 1 2 3 4))
