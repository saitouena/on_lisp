;; 10.1
(defmacro for ((var start stop) &body body)
  (let ((gstop (gensym)))
    `(do ((,var ,start (1+ ,var))
          (,gstop ,stop))
         ((> ,var ,gstop))
       ,@body)))

(defmacro for ((var start stop) &body body)
  `(do ((,var ,start (1+ ,var)))
       ((> ,var ,stop))
     ,@body))

(defmacro for ((var start stop) &body body)
  (let ((gstop (gensym)))
    `(do ((,gstop ,stop)
          (,var ,start (1+ ,var)))
         ((> ,var ,gstop))
       ,@body)))

;; 10.2
(setq x 10)

(+ (setq x 3) x)
;; 右から評価されるなら13

(let ((x 1))
  (for (i x (setq x 13))
    (princ i)))

;;  しかし適切に定義されたforは容易に誤りに気付かせてくれる．  ???
;; 上のsetqの例が何を意図してたのかがよくわからん

;; 右が先に評価されるのに気づくのが難しいということはわかる

;; 10.3

(defmacro nil! (x)                   ; 誤り
  (incf *nil!s*)
  `(setf ,x nil))

;; しかしこの変数の値からnil!の呼ばれた回数が分かると思ったら間違いだ．

;; nil!の呼ばれた回数とは？
;; おそらくソースコード中に現れるnil!の数のことだと思われる

;;  あるマクロ呼び出しは複数回展開され得る（実際しばしばそうだ）．
;; 例えばソースコードの変換を行うプリプロセッサは，式内のマクロ呼び出しを， それを変換すべきかどうか判断する前に展開しなければならないかもしれない．
;; if文などのことかな
;; インタプリタ式に実行して逐次展開していくならtrue節 or false節のいずれかはマクロ展開されないことがある
(if test
    (progn (nil! x) (nil! x))
    (nil! x))

;; インタプリタ式ならtest=trueなら展開は２回
;; インタプリタ式でtest=falseなら展開は１回
;; コンパイラなら3回？

;; わからん

(defmacro string-call (opstring &rest args)              ; 誤り
  `(,(intern opstring) ,@args))

*package*

(in-package :cl)

(defun our+ (x y) (+ x y))

(string-call "OUR+" 2 3)

;; オプショナル引数？誤植ではなさそう
;; However,if we omit the optional package argument, it does so in the current package

;; hyperspec: http://www.lispworks.com/documentation/HyperSpec/Body/f_intern.htm#intern

;; the optional package argument (of `intern`)
;; ということのようだ

;; intern string &optional package => symbol, status

(defpackage hoge
  (:use :cl))

(in-package :hoge)
(cl::our+ 1 2)

(cl::string-call "OUR+" 2 3) ;; HOGE::OUR+ is undefined

(cl::string-call "cl::OUR+" 2 3) ;; The function HOGE::|cl::OUR+| is undefined.

(in-package :cl)

(defmacro string-call (opstring package &rest args) ;; せいかい？
  `(,(intern opstring package) ,@args))

(in-package :hoge)

(cl::string-call "OUR+" "COMMON-LISP-USER" 1 2)
;; => 3

(in-package :cl)

(defun et-al (&rest args)
  (nconc args (list 'et 'al)))

(et-al 'smith 'jones)

(setq greats '(leonardo michelangelo))

(apply #'et-al greats)

greats

;; sbcl ok

;; applyじゃなくても起こりそうな気がするけど...
;; common lispわからん

(defmacro echo (&rest args)
  `',(nconc args (list 'amen)))
;; これとおなじ
(defmacro echo (&rest args)
  `(quote ,(nconc args (list 'amen))))

(defun foo () (echo x))

(foo)

(foo)

;; コンパイルされているので結果は同じ

;; fooは誤った結果を返すだけでなく，実行する度に結果は異なる． マクロが展開される度にfooの定義が変更されているからだ．
;; fooがコンパイルされない場合

;;                  .ここのリストがechoの展開のたびに伸びていくので
(defun foo () (echo x))

(echo x)

(echo x amen)

(echo x amen amen)

;; あるマクロ呼び出しが複数回展開されること
;; これのことかな
;; コンパイルされないパターンを考えればよかったのか

(defmacro echo (&rest args)
  `'(,@args amen))

;; ??? ここから??ENDまでの行が挿入か削除されたようです
;; 誤植なので無視

 
(defmacro crazy (expr) (nconc expr (list t)))

(defun foo () (crazy (list)))

(foo)

;; 可変個の引数を取る関数内でコンシングを避けたいなら， 解決策の一つはマクロを使い，コンシングをコンパイル時にずらす方法だ． マクロをこのように使うことについては，第13章を参照すること．

;; 第3.3節で大まかに触れた原則の一例だ．
;; 3.3を見る

;; 4.4

(defun our-length (x)
  (if (null x)
      0
      (1+ (our-length (cdr x)))))

(defun our-length (x)
  (do ((len 0 (1+ len))
       (y x (cdr y)))
      ((null y) len)))
;; (null y): 停止条件
;; len: return value

(defun ntha (n lst)
  (if (= n 0)
      (car lst)
      (ntha (- n 1) (cdr lst))))

;; 7.10で述べた、関数をマクロにする方法をつかっている
;; やってみるといいかな
(defmacro nthb (n lst)
  `(if (= ,n 0)
       (car ,lst)
       (nthb (- ,n 1) (cdr ,lst))))

(nthb 0 '(a b c))

(nthb 2 '(a b c))
;; インタプリタ的に実行しているので動く

;; nthbを使った関数を描くと。。。？
;; 評価すると無限ループ
(defun nthb-fn (n lst)
  (nthb n lst))

(defmacro nthc (n lst)
  `(do ((n2 ,n (1- n2))
        (lst2 ,lst (cdr lst2)))
       ((= n2 0) (car lst2))))

(nthc 0 '(a b c))

(nthc 2 '(a b c))

;; 再帰関数を呼ぶマクロに変換する

(defmacro nthd (n lst)
  `(nth-fn ,n ,lst))

(defun nth-fn (n lst)
  (if (= n 0)
    (car lst)
    (nth-fn (- n 1) (cdr lst))))

(defmacro nthe (n lst)
  `(labels ((nth-fn (n lst)
              (if (= n 0)
                  (car lst)
                  (nth-fn (- n 1) (cdr lst)))))
     (nth-fn ,n ,lst)))

(defmacro ora (&rest args)
  (or-expand args))

(defun or-expand (args)
  (if (null args)
      nil
      (let ((sym (gensym)))
        `(let ((,sym ,(car args)))
           (if ,sym
               ,sym
               ,(or-expand (cdr args)))))))

;; マクロの引数そのものについて再帰的であり
;; 引数の個数は有限なので必ず停止する
(defmacro orb (&rest args)
  (if (null args)
      nil
      (let ((sym (gensym)))
        `(let ((,sym ,(car args)))
           (if ,sym
               ,sym
               (orb ,@(cdr args)))))))

;; 理解度を確かめるためのかんたんなクイズ
;; なぜorの定義ではletを使っているでしょうか
;; 使わなかった場合どうなるか
