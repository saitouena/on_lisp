(defpackage commons
  (:use common-lisp)
  (:export mac
	   mappend
	   ->
	   ->>
	   mapa-b
	   map0-n
	   map1-n
	   with-gensyms
	   last1
	   single
	   append1
	   conc1
	   mklist
	   group))

(in-package commons)

(defmacro mac (expr)
  (setq *gensym-counter* 0)
  `(pprint (macroexpand-1 ',expr)))

(defun mappend (fn &rest lsts)
  (apply #'append (apply #'mapcar fn lsts)))

(defmacro -> (x &rest forms)
  (labels ((recur (x forms)
	     (if (not forms)
		 x
		 (let* ((f (car forms))
			(threaded (if (listp f)
				      `(,(car f) ,x ,@(cdr f))
				      (list f x))))
		   (recur threaded (cdr forms))))))
    (recur x forms)))

(defmacro ->> (x &rest forms)
  (labels ((recur (x forms)
	     (if (not forms)
		 x
		 (let* ((f (car forms))
			(threaded (if (listp f)
				      `(,@f ,x)
				      (list f x))))
		   (recur threaded (cdr forms))))))
    (recur x forms)))

(defun mapa-b (fn a b &optional (step 1))
  (do ((i a (+ i step)) ;; (varname init how-to-update)
       (result nil)) ;; (varname init)
    ((> i b) (nreverse result))
    (push (funcall fn i) result)))

(defun map0-n (fn n)
  (mapa-b fn 0 n))

(defun map1-n (fn n)
  (mapa-b fn 1 n))

(defmacro with-gensyms (syms &body body)
  `(let ,(mapcar #'(lambda (s)
                     `(,s (gensym ,(symbol-name s))))
                 syms)
     ,@body))

;; list utility
(proclaim '(inline last1 single append1 conc1 mklist))

(defun last1 (lst)
  (car (last lst)))

(defun single (lst)
  (and (consp lst) (not (cdr lst))))

(defun append1 (lst obj)
  (append lst (list obj)))

(defun conc1 (lst obj)
  (nconc lst (list obj)))

(defun mklist (obj)
  (if (listp obj) obj (list obj)))

(defun group (source n)
  (if (zerop n) (error "zero length"))
  (labels ((rec (source acc)
                (let ((rest (nthcdr n source)))
                  (if (consp rest)
                      (rec rest (cons (subseq source 0 n) acc))
                      (nreverse (cons source acc))))))
    (if source (rec source nil) nil)))
