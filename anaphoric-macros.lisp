(load "./commons.lisp")

(defpackage anaphoric-macros
  (:use common-lisp commons))

(in-package anaphoric-macros)

(defmacro aif (test-form then-form &optional else-form)
  `(let ((it ,test-form))
     (if it ,then-form ,else-form)))

(mac (aif (big-long-calculation)
	  (foo it)))

(defmacro awhen (test-form &body body)
  `(aif ,test-form
        (progn ,@body)))

(mac (awhen (big-long-calculation)
       (foo it)
       (bar it)))

(defmacro awhile (expr &body body)
  `(do ((it ,expr ,expr))
     ((not it))
     ,@body))

;; while
(defmacro while (test &body body)
  `(do ()
       ((not ,test))
     ,@body))

(defmacro aand (&rest args)
  (cond ((null args) t)
        ((null (cdr args)) (car args)) ;; ひとつだけのときはそのままでok
	                               ;; (t or nil でなく、評価した値をそのまま返したいので)
        (t `(aif ,(car args) (aand ,@(cdr args))))))

(mac (aand (owner x) (address it) (town it)))
(mac (owner x))

(defmacro acond (&rest clauses)
  (if (null clauses)
      nil
      (let ((cl1 (car clauses))
            (sym (gensym)))
        `(let ((,sym ,(car cl1)))
           (if ,sym
               (let ((it ,sym)) ,@(cdr cl1))
               (acond ,@(cdr clauses)))))))

(cond (3)) ;; => 3 なのでacondも3を返すべきだが、バグっている

;; 修正版
(defmacro acond-fixed (&rest clauses)
  (if (null clauses)
      nil
      (let ((cl1 (car clauses))
            (sym (gensym)))
	(if (cdr cl1)
	    `(let ((,sym ,(car cl1)))
	       (if ,sym
		   (let ((it ,sym)) ,@(cdr cl1))
		   (acond ,@(cdr clauses))))
	    `(let ((,sym ,(car cl1)))
	       (if ,sym
		   ,sym
		   (acond ,@(cdr clauses))))))))

(acond (3))
(acond-fixed (3))

;; itの束縛はその次のテスト式もスコープに含んでしまうだろう．

(defmacro bad-acond (&rest clauses)              ; 誤り
  (if (null clauses)
      nil
      (let ((cl1 (car clauses)))
        `(let ((it ,(car cl1)))
           (if it
               (progn ,@(cdr cl1))
               (acond ,@(cdr clauses)))))))

;; これが動いてしまう。
;; acondではうごかない
(bad-acond (nil it)
	   ((progn (print it) ;; it = nil (nil it) から来てしまっている
		   t)
	    it))

(defmacro alambda (parms &body body)
  `(labels ((self ,parms ,@body))
     #'self))

(defun count-instances (obj lists)
  (labels ((instances-in (list)
             (if list
                 (+ (if (eq (car list) obj) 1 0)
                    (instances-in (cdr list)))
                 0)))
    (mapcar #'instances-in lists)))

(count-instances 'a '((a b c) (d a r p a) (d a r) (a a)))

(funcall (alambda (x) (if (= x 0) 1 (* x (self (1- x))))) 5)
;; clojureのrecurっぽい

(defun count-instances (obj lists)
  (mapcar (alambda (list)
            (if list
                (+ (if (eq (car list) obj) 1 0)
                   (self (cdr list)))
                0))
          lists))

(defmacro ablock (tag &rest args)
  `(block ,tag
     ,(funcall (alambda (args)
                 (case (length args)
                   (0 nil) ;; (ablock tag) のときだけのため
                   (1 (car args))
                   (t `(let ((it ,(car args)))
                         ,(self (cdr args))))))
               args)))

(mac (ablock north-pole
	     (princ "ho ")
	     (princ it)
	     (princ it)
	     (return-from north-pole)))
