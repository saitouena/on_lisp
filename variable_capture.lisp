;; utility
(defmacro mac (expr)
  `(pprint (macroexpand-1 ',expr)))

;; cf. destructual bind
(defmacro for ((var start stop) &body body)          ; 誤り
  `(do ((,var ,start (1+ ,var))
        (limit ,stop))
     ((> ,var limit))
     ,@body))

(for (x 1 5)
  (princ x))

(for (limit 1 5)
  (princ limit)) ;; error: LIMIT occurs more than once in the LET with SBCL

(mac (for (limit 1 5)
       (princ limit)))

(let ((limit 5))
  (for (i 1 10)
    (when (> i limit) ;; このlimitはletで導入されたものであったほしいが、そうはならない。forに生成されたlimit(=10)を指してしまう
      (princ i))))

;; expected: 6789
;; got: nil

(mac (for (i 1 10)
       (when (> i limit)
	 (princ i))))

;; forは、引数body内部のシンボルが補足されてしまう例。（マクロ引数の補足）

;; 9.2

(defvar w nil)

(defmacro gripe (warning)                        ; 誤り
  `(progn (setq w (nconc w (list ,warning)))
          nil))

(defun sample-ratio (v w)
  (let ((vn (length v)) (wn (length w)))
    (if (or (< vn 2) (< wn 2))
      (gripe "sample < 2")
      (/ vn wn))))



(let ((lst '(b)))
    (sample-ratio nil lst)
    lst) ;; '(b) is broken

;; 9.3

;; 厳密ではないものの変数捕捉を調べる規則を学ぶ

;; 補足可能
;; (a) マクロ展開の骨格内でそのシンボルがフリーなまま現れているか，
;; (b) マクロに渡された引数が束縛または評価される骨格の一部にそのシンボルが束縛されている

;; bの日本語がわかりづらい

(defmacro cap1 ()
  '(+ x 1))
;; 骨格
;; 空白を?と書くことにする
(+ x 1)
;; x is free -> case (a)

(defmacro cap2 (var)
  `(let ((x ...)
	 (,var ...))
     ...))
;; 骨格

(let ((x ...)
      (? ...))
  ...)
;; case (b)
;; xは捕捉
;; 「マクロに渡された引数」 = var
;; 「束縛または評価される骨格の一部に」 このケースは束縛される骨格
;; 「そのシンボルが」 = x
;; varが束縛される骨格の一部にxが束縛されている
;; varがxだったときにだめになる


;; cap1: gripe
;; cap2,cap3, cap4: var = x のとき

(defmacro safe1 (var)
  `(progn (let ((x 1))
            (print x))
          (let ((,var 1))
            (print ,var))))
;; xは補足可能ではない
;; (b)のケースに当てはまらないことを確認
;; 「マクロに渡された引数が束縛または評価される骨格」
(let ((,var 1))
  (print ,var))
;;ここにｘは束縛されていないのでおｋ


;; 心配する必要があるのは骨格内の変数の束縛だけだということも気を付けて欲しい
(defmacro safe3 (var &body body)
  `(let ((,var ...))
     ,@body))
;; 引数以外にシンボルがが出てこないのでokということ...のはず

;; bad for
(defmacro for ((var start stop) &body body)          ; 誤り
  `(do ((,var ,start (1+ ,var))
	(limit ,stop)) ;; limitは補足可能
       ((> ,var limit))
     ,@body))

(mac (for (limit 1 5)
       (printc limit)))

(let ((limit 0))
  (for (x 1 10)
    (incf limit x)) ;; limitはforに束縛されるlimitを指している. letで作られたのものを指してはいない
  limit)

(let ((limit 0))
  (do ((x 1 (1+ x)) ;; x = 3
       (limit 10)) ;; limit = 16
      ((> x 10))
    (incf limit x))
  limit)

(let ((x 1)) (list x))
;; let もマクロ
;; xはlet内で束縛されるのでxは補足されているといえば補足されている

(defun flatten (x)
  (labels ((rec (x acc)
                (cond ((null x) acc)
                      ((atom x) (cons x acc))
                      (t (rec (car x) (rec (cdr x) acc))))))
    (rec x nil)))

;; ３個の規則というのは、(b)が束縛と評価の２パターンがあるので1+2=3
(defmacro pathological (&body body)                 ; 誤り
  (let* ((syms (remove-if (complement #'symbolp)
                          (flatten body)))
         (var (nth (random (length syms))
                   syms)))
    `(let ((,var 99))
       ,@body)))

(let ((x 10)
      (y 2))
  (pathological (+ x y)))

(remove-if (complement #'symbolp)
	   (flatten '(+ x y z 3 4)))

;; bodyに現れるランダムなシンボルを選んで束縛する
;; 骨格内にシンボルがでてこないけど束縛される

;; 9.5
;; bad
(defmacro before (x y seq)
  `(let ((seq ,seq))
     (< (position ,x seq)
	(position ,y seq))))
;; 規則に当てはめてみる
;; seqが補足可能
;; 引数xが評価される骨格の一部にseqが束縛されているので
;; なんちゃらインジェクションみたい
(mac (before (progn (setq seq '(b a)) 'a)
	     'b
	     '(a b)))

(let ((seq '(a b)))
  (< (position (progn (setq seq '(b a)) 'a) seq) ;; seqが(b a)になっているので => 1
     (position 'b seq))) ;; => 0
;; 左の引数から順番に評価されるという仮定で見ちゃったけど...

;; good
(defmacro before (x y seq)
  `(let ((xval ,x) (yval ,y) (seq ,seq))
     (< (position xval seq)
        (position yval seq))))

(mac (before (progn (setq seq '(b a)) 'a)
	     'b
	     '(a b))) ;; => T

;; 残念なことに，letを使う技が通用する状況は多くない
;; 1. 捕捉の危険のある引数はきっかり1回だけ評価されるべきで，
;; 2. マクロ骨格に生成された束縛のスコープ内ではどの引数も評価の必要はない，

;; for macroをみる
;; 1 => body,varは何回も評価される必要がある
;; 2 => varは新しく束縛されたvarとlimitのスコープ内で評価される必要がある
;; なのであてはまらない

;; good for
(defmacro for ((var start stop) &body body)
  `(do ((b #'(lambda (,var) ,@body))
	(count ,start (1+ count))
	(limit ,stop))
       ((> count limit))
     (funcall b count)))

;; これもうまくうごく
(let ((limit 5))
  (for (i 1 10)
    (when (> i limit)
      (princ i))))

(let ((limit 0))
  (mac (for (x 1 10)
	 (incf limit x))) ;; limitはforに束縛されるlimitを指している. letで作られたのものを指してはいない
  limit)
;; ok

;; 9.6 GenSym
(gensym)
(print (gensym))

(defmacro for ((var start stop) &body body)
  `(do ((,var ,start (1+ ,var))
        (limit ,stop))
     ((> ,var limit))
     ,@body))

;; good for
(defmacro for ((var start stop) &body body)
  (let ((gstop (gensym)))
    `(do ((,var ,start (1+ ,var))
          (,gstop ,stop))
         ((> ,var ,gstop))
       ,@body)))

(print *gensym-counter*)

(let* ((c *gensym-counter*)
       (x (gensym))
       (y (progn
	    (setq *gensym-counter* c)
	    (gensym))))
  (print x)
  (print y)
  (print (eq x y)))
;; => nil

;; 9.7 package
;; 付録のところをよむ

;; clojureのnsみたいな感じかな

;; 9.8
;; 関数やブロックについて
(setq hoge (lambda (x) (+ x 1)))
(funcall #'hoge 1)
(funcall #'(lambda (x) (+ x 1)) 1)

(defun fn (x) (+ x 1))
(funcall fn 1)

(defmacro mac (x) `(fn ,x))
(mac 10) ;; (fn 10)

(labels ((fn (y) (- y 1)))
  (print (mac 10)))

(block nil
  (list 'a
	(do ((x 1 (1+ x)))
	    (nil) ;; 無限ループ
	  (if (> x 5)
	      (return-from nil x) ;; (return-from nil 6)
	      (princ x)))))
;; actual: (a 6) , expected 6
(mac (do ((x 1 (1+ x)))
	    (nil) ;; 無限ループ
	  (if (> x 5)
	      (return-from nil x) ;; (return-from nil 6)
	      (princ x))))

(print nil)

(print (block nil
	 (list 'a (for (i 1 10)
		    (if (= i 3)
			(return-from nil i))))))

(print (block out-of-for
	 (list 'a (for (i 1 10)
		    (if (= i 3)
			(return-from out-of-for i))))))

;; 9.9

;; Schemeの健全なマクロ
;; http://www.nct9.ne.jp/m_hiroi/func/abcscm22.html
;; 自動的にgensymされる
