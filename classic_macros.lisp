(load "./commons.lisp")

(defpackage classic-macros
  (:use common-lisp commons))

(in-package classic-macros)

(commons:mac (let ((x 1)) x))

(defmacro our-let (binds &body body)
  `((lambda ,(mapcar #'(lambda (x)
                         (if (consp x) (car x) x))
                     binds)
      ,@body)
    ,@(mapcar #'(lambda (x)
                   (if (consp x) (cadr x) nil))
	      binds)))

(commons:mac (our-let ((x 1) (y 2))
	       (+ x y)))

(commons:mac (our-let (x (y 2))
	       (list x y)))

(defmacro when-bind ((var expr) &body body)
  `(let ((,var ,expr))
     (when ,var
       ,@body)))

(when-bind (x nil)
  (print x))

(when-bind (x t)
  (print x))

(defmacro when-bind* (binds &body body)
  (if (null binds)
      `(progn ,@body)
      `(let (,(car binds))
         (if ,(caar binds)
             (when-bind* ,(cdr binds) ,@body)))))

(commons:mac (when-bind* ((x (find-if #'consp '(a (1 2) b)))
			  (y (find-if #'oddp x)))
	       (+ y 10)))

(defmacro with-gensyms (syms &body body)
  `(let ,(mapcar #'(lambda (s)
                     `(,s (gensym)))
                 syms)
     ,@body))

(with-gensyms (x y z)
  (print x)
  (print y)
  (print z))

(defmacro condlet (clauses &body body)
  (let ((bodfn (gensym "bodfn"))
        (vars (mapcar #'(lambda (v) (cons v (gensym "var")))
                      (remove-duplicates
		       (mapcar #'(lambda (c) (if (consp c) (car c) c))
			       (commons:mappend #'cdr clauses))))))
    `(labels ((,bodfn ,(mapcar #'car vars)
		,@body))
       (cond ,@(mapcar #'(lambda (cl)
			   (condlet-clause vars cl bodfn))
		       clauses)))))

(defun condlet-clause (vars cl bodfn)
  ;; (car cl) : condの条件
  `(,(car cl) (let ,(mapcar #'cdr vars)
                (let ,(condlet-binds vars cl)
		  ;; bodfnにapplyする
                  (,bodfn ,@(mapcar #'cdr vars))))))

;; 内側のletのbindsを作る
(defun condlet-binds (vars cl)
  (mapcar #'(lambda (bindform)
              (if (consp bindform)
                  (cons (cdr (assoc (car bindform) vars))
                        (cdr bindform))
		  (cdr (assoc bindform vars))))
          (cdr cl)))
(condlet-binds `((X . ,(gensym "var")) (Y . ,(gensym "var")) (Z . ,(gensym "var")))
	       '((= 1 1) y (x (princ 'd))))

(remove-duplicates'(x y y x x z))
(mac (condlet (((= 1 2) (x (princ 'a)) (y (princ 'b)))
	       ((= 1 1) (y (princ 'c)) (x (princ 'd)))
	       (t       (x (princ 'e)) (z (princ 'f))))
       (list x y z)))

(condlet (((= 1 2) (x (princ 'a)) (y (princ 'b)))
	  ((= 1 1) y (x (princ 'd)))
	  (t       (x (princ 'e)) (z (princ 'f))))
  (list x y z))

;; clauses
'(((= 1 2) (x (princ 'a)) (y (princ 'b)))
  ((= 1 1) (y (princ 'c)) (x (princ 'd)))
  (t       (x (princ 'e)) (z (princ 'f))))

(mapcar #'(lambda (v) (cons v (gensym)))
                      (remove-duplicates
		       (mapcar #'car
			       (commons:mappend #'cdr '(((= 1 2) (x (princ 'a)) (y (princ 'b)))
							((= 1 1) (y (princ 'c)) (x (princ 'd)))
							(t       (x (princ 'e)) (z (princ 'f))))))))

(commons:mappend #'cdr '(((= 1 2) (x (princ 'a)) (y (princ 'b)))
			 ((= 1 1) (y (princ 'c)) (x (princ 'd)))
			 (t       (x (princ 'e)) (z (princ 'f)))))
;; これのmapcar #'carをとる
(mapcar #'car (commons:mappend #'cdr '(((= 1 2) (x (princ 'a)) (y (princ 'b)))
				       ((= 1 1) (y (princ 'c)) (x (princ 'd)))
				       (t       (x (princ 'e)) (z (princ 'f))))))
;; 重複があるのでremote-duplicatesする
(remove-duplicates'(x y y x x z))
;; threading macroで書き直したい衝動に駆られる

(commons:->> '(((= 1 2) (x (princ 'a)) (y (princ 'b)))
	       ((= 1 1) (y (princ 'c)) (x (princ 'd)))
	       (t       (x (princ 'e)) (z (princ 'f))))
	     (commons:mappend #'cdr)
	     (mapcar #'car)
	     (remove-duplicates)
	     (mapcar #'(lambda (v) (cons v (gensym)))))

(commons:-> 1
	    (+ 1)
	    (* 2)
	    1+)

(defmacro condlet (clauses &body body)
  (let ((bodfn (gensym))
        (vars (commons:->> clauses
			   (commons:mappend #'cdr)
			   (mapcar #'car)
			   (remove-duplicates)
			   (mapcar #'(lambda (v) (cons v (gensym)))))))
    `(labels ((,bodfn ,(mapcar #'car vars)
		,@body))
       (cond ,@(mapcar #'(lambda (cl)
			   (condlet-clause vars cl bodfn))
		       clauses)))))

;; cl: ((= 1 2) (x (princ 'a)) (y (princ 'b))) etc.
;; vars: ((Y . #:G) (X . #:G) (Z . #:G))

;; labels bodfn のところは変数補足を避けるテクニックのlambdaでbodyを包むテク？
;; http://www.asahi-net.or.jp/~kc7k-nd/onlispjhtml/variableCapture.html

;; bodfnを使うとbodyの中のx,y,zを探索する必要がなくなってよいらしい
;; 使えそう

;; bindform = (x (princ 'a))
;; vars = ((Y . G1) (X . G2) (Z . G3))
;; としてみる
(if (consp '(x (princ 'a)))
    (cons (cdr (assoc (car '(x (print 'a))) '((Y . G1) (X . G2) (Z . G3))))
	  (cdr '(x (princ 'a)))))


;; 11.2
(with-open-file (s "dump" :direction :output)
  (princ 99 s))
;; file "dump" が生成されている

;; ignore-errors
;; http://www.lispworks.com/documentation/HyperSpec/Body/m_ignore.htm#ignore-errors

(ignore-errors
  (/ 1 0)
  1)

(mac (ignore-errors
       2
       1))
;; ignore-errors

(mac (HANDLER-CASE (PROGN 2 1) (ERROR (CONDITION) (VALUES NIL CONDITION))))

;; その引数が新しいコンテキスト内で評価されるからだ．
;; 本の脚注をみる
;; 「エラーを無視する」というコンテキスト

(setq x 'a)

(unwind-protect
     (progn (princ "What error?")
	    (error "This error."))
  (setq x 'b))

x

(mac (prog1 1 2 3))

(let ((temp *db*))
  (setq *db* db)
  (lock *db*)
  (prog1 (eval-query q)
    (release *db*)
    (setq *db* temp) ;; 仮想のコードなので意味を追求しすぎても良くないとは思うけどtempとはなんぞや
    ))

;; (eval-query db q)
;; のように呼び出せたほうが嬉しそうだが
;; dynamic varで受け渡していると考えられる。
;; このインターフェースだと並行のdbアクセスがかけないけどいいのかな

;; pure macro
;; 上のコードを置き換えただけ
(defmacro with-db (db &body body)
  (let ((temp (gensym)))
    `(let ((,temp *db*))
       (unwind-protect
         (progn
           (setq *db* ,db)
           (lock *db*)
           ,@body)
         (progn
           (release *db*)
           (setq *db* ,temp))))))

;; macro and function
(defmacro with-db (db &body body)
  (let ((gbod (gensym)))
    `(let ((,gbod #'(lambda () ,@body)))
       (declare (dynamic-extent ,gbod))
       (with-db-fn *db* ,db ,gbod))))

(defun with-db-fn (old-db new-db body)
  (unwind-protect
    (progn
      (setq *db* new-db)
      (lock *db*)
      (funcall body))
    (progn
      (release *db*)
      (setq *db* old-db))))

;; 条件付き評価
(defmacro if3 (test t-case nil-case ?-case)
  `(case ,test
     ((nil) ,nil-case)
     (?       ,?-case)
     (t       ,t-case)))

(case 6
  ((2 1 3) :hoge)
  ((4 5 6) :fuga)
  (otherwise :piga))

(while (not sick)
  (if3 (cake-permitted)
       (eat-cake)
       (throw 'tantrum nil) ;; かんしゃく
       (plead-insistently) ;; 「しつこく弁護する」
       ))

;; キーnilはリストに括らなければならない． nil単独では意味があいまいになるからだ．

(case nil
  (nil 1) ;; xが何でもここが評価されることはない
  ((nil) 2))
;; =>
(case :e
  ((()) 1) ;; xが何でもここが評価されることはない
  (t 2))
(eq nil ())

(case nil
  (nil 1)
  ((nil) 2))

(defmacro nif (expr pos zero neg)
  (let ((g (gensym)))
    `(let ((,g ,expr))
       (cond ((plusp ,g) ,pos)
             ((zerop ,g) ,zero)
             (t ,neg)))))

(mapcar #'(lambda (x)
	    (nif x 'p 'z 'n))
	'(0 1 -1))

;; べつの実装方法
(defmacro nif (expr pos zero neg)
  `(case (truncate (signum ,expr))
     (1 ,pos)
     (0 ,zero)
     (-1 ,neg)))

;; 冗長だが効率的
(let ((x (foo)))
  (or (eql x (bar)) (eql x (baz))))

;; 簡潔だが非効率的
(member (foo) (list (bar) (baz)))

;; 理由1 listの呼び出しが不要なコンシングを生む

;; 理由２
(member (foo) (list (baz) (needs-too-long-time-to-get-bar)))

;; マクロで解決

(defmacro in (obj &rest choices)
  (let ((insym (gensym)))
    `(let ((,insym ,obj))
       (or ,@(mapcar #'(lambda (c) `(eql ,insym ,c))
		     choices)))))

(mapcar #'(lambda (c) `(eql x ,c))
	'(a b c))

(commons:mac (in x a b c))

(defmacro inq (obj &rest args)
  `(in ,obj ,@(mapcar #'(lambda (a)
			  `',a) ;; = `(quote ,a)
		      args)))

;; setの代わりにsetqを使うようなものだ
;; setを知らず
;; http://www.lispworks.com/documentation/HyperSpec/Body/f_set.htm#set

;; (setq x 2) == (set 'x 2)
;; かなぁ。setは関数らしい

;; in-if

(progn (side-effect)
       #'(lambda (x) x))

(defmacro in-if (fn &rest choices)
  (let ((fnsym (gensym)))
    `(let ((,fnsym ,fn))
       (or ,@(mapcar #'(lambda (c)
			 `(funcall ,fnsym ,c))
		     choices)))))

(commons:mac (in-if #'oddp 2 4 6))


;; in-if <-> some
;; in    <-> member

;; 本の例がおかしい気がする
;; inの例が出てこないやんけ

;; 意味がわかるように直してみる

(member x (list a b))
;; <=>
(in x a b)

(some #'oddp (list a b))
;; <=>
(in-if #'oddp a b)

;; some -> in-if, member -> inと書き換えていくだけで性能がアップした

;; >case

(defmacro >case (expr &rest clauses)
  (let ((g (gensym)))
    `(let ((,g ,expr))
       (cond ,@(mapcar #'(lambda (cl) (>casex g cl))
		       clauses)))))

(defun >casex (g cl)
  (let ((key (car cl)) (rest (cdr cl)))
    (cond ((consp key) `((in ,g ,@key) ,@rest))
          ((inq key t otherwise) `(t ,@rest))
          (t (error "bad >case clause")))))

;; (x y)が関数やマクロの呼び出しなのか2つのキーから成るリストなのか判断が付かない．
;; 曖昧さをなくすため，（t以外の）キーは， 1つしかない場合でも必ずリストに括られていなければならない．

;; こういうこと

;; case節のcarで関数呼び出しをしたいとき

(mac (>case 1
	    (((+ 1 2)) :three)
	    (((- 1 2)) :minus-one)
	    (((+ 0 1)) :one)
	    (otherwise :otherwise)))

(mac (>case 1
	    ((+ 1 2) :three)
	    ((- 1 2) :minus-one)
	    ((+ 0 1) :one)
	    (otherwise :otherwise)))

(setq x 1 y 2)

(case 'x
  (x :x)
  (y :y))

(commons:mac (>case 1
		    ((x) :x) ;; xを評価してほしいときにできる
		    ((y) :y)))
;; clause ::= (keys form) keysはlist前提(t,otherwiseだけ特別)

;;caseの場合，曖昧であるためにnilは節のcar部になってはいけなかった．
;;>caseでは，nilは節のcar部として曖昧ではない．
;;しかしそうすると節の残りが決して評価されないことになる．

;; 何が言いたいのかよくわからない

;; hyperspecの例
;; http://www.lispworks.com/documentation/HyperSpec/Body/m_case_.htm#case
(dolist (k '(1 2 3 :four #\v () t 'other))
  (format t "~S ~S~%"
	  k
	  (case k ((1 2) 'clause1)
		(3 'clause2)
		(nil 'no-keys-so-never-seen)
		((nil) 'nilslot)
		((:four #\v) 'clause4)
		((t) 'tslot)
		(otherwise 'others))))

(consp nil)

(>case nil
       (() :nil)
       (otherwise :not-nil));; errorだが...

(>case nil
       ((nil) :nil)
       (otherwise :not-nil))

(case nil
  ((nil) :nil)
  (otherwise :not-nil))

;; indentがあれだけど、>caseの定義の&restを&bodyにするとインデントがマシになりそう

(defmacro >case (expr &body clauses)
  (let ((g (gensym)))
    `(let ((,g ,expr))
       (cond ,@(mapcar #'(lambda (cl) (>casex g cl))
		       clauses)))))

;; 以下いろいろ試しているけど結局よくわからず

(>case nil
  ((nil) :nil)
  (otherwise :not-nil))

(case nil
  (nil :nil)
  (otherwise :not-nil)) ;; => :NOT=NIL

(case nil
  (nil :nil)
  (otherwise :not-nil)) ;; => :NOT=NIL

(case nil
  (() :nil)
  (otherwise :not-nil)) ;; => :NOT-NIL

(case nil
  ((nil) :nil)
  (otherwise :not-nil))

;; 11.4 反復
(defmacro forever (&body body)
  `(do ()
       (nil)
     ,@body))

(forever
  ;; 何かする
  (if flag
      (return-from nil)))

(defmacro while (test &body body)
  `(do ()
     ((not ,test))
     ,@body))

(defmacro till (test &body body)
  `(do ()
     (,test)
     ,@body))

(defmacro for ((var start stop) &body body)
  (let ((gstop (gensym)))
    `(do ((,var ,start (1+ ,var))
          (,gstop ,stop))
       ((> ,var ,gstop))
       ,@body)))

(defmacro do-tuples/o (parms source &body body)
  (if parms
      (let ((src (gensym)))
        `(prog ((,src ,source))
               (mapc #'(lambda ,parms ,@body)
                     ,@(map0-n #'(lambda (n)
                                    `(nthcdr ,n ,src))
                                (1- (length parms))))))))

(defmacro do-tuples/c (parms source &body body)
  (if parms
      (with-gensyms (src rest bodfn)
	(let ((len (length parms)))
	  `(let ((,src ,source))
	     (when (nthcdr ,(1- len) ,src)
	       (labels ((,bodfn ,parms ,@body))
		 (do ((,rest ,src (cdr ,rest)))
		     ((not (nthcdr ,(1- len) ,rest))
		      ;; 最初と最後にまたがるやつ
		      ,@(mapcar #'(lambda (args)
				    `(,bodfn ,@args))
				(dt-args len rest src))
		      nil)
		   (,bodfn ,@(map1-n #'(lambda (n)
					 `(nth ,(1- n)
					       ,rest))
				     len))))))))))

;; rest, srcはシンボルとして渡されることに注意
;; rest-sym, src-symとかにするとこんがらがらないと思う
;; 長さlenのリストをlen-1こ生成する
(defun dt-args (len rest src)
  (map0-n #'(lambda (m) ;; m = 
              (map1-n #'(lambda (n)
                          (let ((x (+ m n)))
                            (if (>= x len)
                                `(nth ,(- x len) ,src) ;; (- x len) は x mod lenを実質取っている
                                `(nth ,(1- x) ,rest) ;; restの先頭 xをとる map1-nを使ったせいで1-する必要が出ているので、はじめからmap0-nを使えばよかったのではという気持ち
				)))
                      len))
	  ;; なんで-2するんだろう
	  ;; 多分先頭0,nを覗いている？
          (- len 2)))

;;; clojureとかlist内包表記が書ける言語だったらそれでdt-argsを書き直したほうが良いような
;; (for [m (range 0 (- len 1))]
;;   (for [n (range 1 (+ len 1))
;;         :let [x (+ m n)]]
;;     (if (>= x len)
;;       `(nth ~(- x len) ~src)
;;       `(nth ~(1- x) ~rest))))

;; map0-nで書き直したほうが良い
(defun dt-args (len rest-sym src-sym)
  (map0-n #'(lambda (m)
              (map0-n #'(lambda (n)
                          (let ((x (+ m n)))
                            (if (>= x len)
                                `(nth ,(- x len) ,src-sym)
                                `(nth ,x ,rest-sym)
				)))
                      (1- len)))
          (- len 2)))

(dt-args 3 'rest 'src)



;; do-tuples/o をみる

(mac (do-tuples/o (x y) '(a b c d)
       (print (list x y))))

;; 展開系をみるとあまり難しくなさそうに見える
;; mapc: http://www.lispworks.com/documentation/HyperSpec/Body/f_mapc_.htm#mapc
;; mapc is like mapcar except that the results of applying function are not accumulated. The list argument is returned.
;; 副作用を期待して使う？

;; 整形してみるとわかる

(PROG ((G657 '(A B C D)))
   (MAPC #'(LAMBDA (X Y) (PRINT (LIST X Y)))
	 (NTHCDR 0 G657)
	 (NTHCDR 1 G657)))

(nthcdr 0 '(A B C D))
(nthcdr 1 '(A B C D))

;; map系に複数のリストを渡すと、一番短いやつのぶんだけ実行されることを利用している

(mapcar #'cons '(1 2 3 4) '(5 6))

;; map0-n is n-inclusive

;; do-tuples/cをみる

(mac (do-tuples/c (x y) '(a b c d)
       (print (list x y))))

;; 展開系を見てみる

(LET ((G659 '(A B C D)))
  (WHEN (NTHCDR 1 G659)
    (LABELS ((G661 (X Y)
               (PRINT (LIST X Y))))
      (DO ((G660 G659 (CDR G660)))
          ((NOT (NTHCDR 1 G660)) (G661 (NTH 0 G660) (NTH 0 G659)) NIL)
        (G661 (NTH 0 G660) (NTH 1 G660))))))

;; parms: (x y)
;; source: '(a b c d)
;; body: (print (list x y))
;; with-gensymsを使うと見づらいので、
;; symbol-nameをつかうwith-gensymsにする
(symbol-name 'hoge)
(defmacro do-tuples/c (parms source &body body)
  (if parms
      (with-gensyms (src rest bodfn)
	(let ((len (length parms))) ;; len: 2
	  `(let ((,src ,source)) ;; '(a b c d)
	     (when (nthcdr ,(1- len) ,src) ;; sourceの長さは実行時にしかわからないので、１回も反復をしないこともある
	       　　　　　　　　　　　　　　　　;; そのときはwhenでチェックして抜ける
	       (labels ((,bodfn ,parms ,@body))
		 (do ((,rest ,src (cdr ,rest)))
		     ((not (nthcdr ,(1- len) ,rest))
		      ;; 最初と最後をとるやつ
		      ,@(mapcar #'(lambda (args)
				    `(,bodfn ,@args))
				(dt-args len rest src))  ; 注意: dt-argsの計算はマクロ展開時に呼び出されるので、rest,srcはただのシンボルとして渡されている
		      nil)
		   ;; ,@(map1-n ...): 0,1,2,...,len-1について、(nth i rest)をする
		   (,bodfn ,@(map1-n #'(lambda (n)
					 `(nth ,(1- n)
					       ,rest))
				     len))))))))))

(nthcdr 2 '(a b c d))

(nthcdr 2 '(b c d))

(nthcdr 2 '(c d))

(nthcdr 2 '(d))

;; 前と後ろが繋がったやつをとるやつを作る関数 dt-args
(dt-args 3 'rest 'src)

(map0-n #'(lambda (m)
	    (map1-n #'(lambda (n)
			(let ((x (+ m n)))
			  (if (>= x 2)
			      `(nth ,(- x 2) src)
			      `(nth ,(1- 2) rest))))
		    2))
	(- 2 2))

(do-tuples/o (x y z) '(a b c d)
  (print (list x y z)))

(do-tuples/c (x y z) '(a b c d)
  (print (list x y z)))

(do-tuples/o (x) '(a b c d)
  (print x))

(mac (do-tuples/c (x) '(a b c d)
       (print x)))

(do-tuples/c (x) (gen-list)
  (print x))

(mac (do-tuples/c (x y z w u) (gen-list)
       (print (list x y z))))

(nthcdr 4 '(a b c d e f))

;; '(a b c d) をこっそり '(a b c d a b c d)にする方針では実装していない
;; この方がかんたんなのだが最適化をしていて難しくなっている

;; かんたんに書く
;; こっちを先に見た方が良い気がする
(defmacro do-tuples/c (parms source &body body)
  (if parms
      (with-gensyms (original-src src rest i bodfn)
	(let ((len (length parms)))
	  `(let* ((,original-src ,source)
		  (,src (append ,original-src ,original-src)))
	     (when (nthcdr ,(1- len) ,original-src)
	       (labels ((,bodfn ,parms ,@body))
		 (do ((,rest ,src (cdr ,rest))
		      (,i 0 (1+ ,i)))
		     ((not (nthcdr ,i ,original-src)) nil)
		   (,bodfn ,@(map1-n #'(lambda (n) `(nth ,(1- n) ,rest)) len))))))))))

(mac (do-tuples/c (x y z) '(a b c d)
       (print (list x y z))))

;; 11.5 複数の値に渡る反復

;;; mvdo*

(defmacro mvdo* (parm-cl test-cl &body body)
  (mvdo-gen parm-cl parm-cl test-cl body))

(defun mvdo-gen (binds rebinds test body)
  ;; mvdo*については、binds = rebinds
  (if (null binds)
      (let ((label (gensym)))
        `(prog nil
               ,label
               (if ,(car test)
                   (return (progn ,@(cdr test))))
               ,@body
               ,@(mvdo-rebind-gen rebinds) ;; 上から順番に評価
               (go ,label)))
      (let ((rec (mvdo-gen (cdr binds) rebinds test body)))
        (let ((var/s (caar binds)) (expr (cadar binds))) ;; bind = (var/s expr rebind)
          (if (atom var/s)
              `(let ((,var/s ,expr)) ,rec)
              `(multiple-value-bind ,var/s ,expr ,rec))))))

(defun mvdo-rebind-gen (rebinds)
  (cond ((null rebinds) nil)
        ((< (length (car rebinds)) 3)
         (mvdo-rebind-gen (cdr rebinds)))
        (t (cons (list (if (atom (caar rebinds))
                          'setq
                          'multiple-value-setq)
                      (caar rebinds) ;; var/s
                      (third (car rebinds)) ;; how to rebind
		      )
                (mvdo-rebind-gen (cdr rebinds))))))

(mvdo* ((x 1 (1+ x))
	((y z) (values 0 0) (values z x)))
    ((> x 5) (list x y z))
  (princ (list x y z)))

(mac (mvdo* ((x 1 (1+ x))
	     ((y z) (values 0 0) (values z x)))
	 ((> x 5) (list x y z))
       (princ (list x y z))))

;; interactive game
;; px,py : given
(mvdo* (((px py) (pos player)   (move player mx my)) ;; mx, myは下にあるのを参照している
        ((x1 y1) (pos obj1)     (move obj1 (- px x1)
                                      (- py y1)))
        ((x2 y2) (pos obj2)     (move obj2 (- px x2)
                                      (- py y2)))
        ((mx my) (mouse-vector) (mouse-vector))
        (win     nil            (touch obj1 obj2))
        (lose    nil            (and (touch obj1 player)
                                     (touch obj2 player))))
       ((or win lose) (if win 'win 'lose))
       (clear)
       (draw obj1)
       (draw obj2)
       (draw player))

;;; またローカル変数を並列して束縛するmvdoを書くこともできる．


(defmacro mvpsetq (&rest args)
  (let* ((pairs (group args 2)) ;; 2の倍数でないパターンは想定されてなさそう
         (syms (mapcar #'(lambda (p)
                           (mapcar #'(lambda (x) (gensym))
                                   (mklist (car p))))
                       pairs)))
    (labels ((rec (ps ss)
                  (if (null ps)
                      `(setq
                         ,@(mapcan #'(lambda (p s) ;; map + concat
                                        (shuffle (mklist (car p))
                                                 s))
                                    pairs syms))
                      (let ((body (rec (cdr ps) (cdr ss))))
                        (let ((var/s (caar ps))
                              (expr (cadar ps)))
                          (if (consp var/s)
                              `(multiple-value-bind ,(car ss)
                                 ,expr
                                 ,body)
                              `(let ((,@(car ss) ,expr))
                                 ,body)))))))
      (rec pairs syms))))

;; interleave, zip的な関数
(defun shuffle (x y)
  (cond ((null x) y)
        ((null y) x)
        (t (list* (car x) (car y)
                  (shuffle (cdr x) (cdr y))))))

(shuffle '(a b c) '(1 2 3 4))

(mvpsetq (hoge fuga) (values 1 2) (piga poga pige) (values 3 4 5))
(list hoge fuga piga poga pige)
(mac (mvpsetq (hoge fuga) (values 1 2) (piga poga pige) (values 3 4 5)))

(setq x 10)

(mvpsetq x 2 (y z) (values x (+ x 1)))
(list x y z)
(mac (mvpsetq x 2 (y z) (values x (+ x 1))))

;; pairs
(group (cdr '(mvpsetq x 2 (y z) (values x (+ x 1)))) 2)

;; syms
(mapcar #'(lambda (p)
	    (mapcar #'(lambda (x) (gensym))
		    (mklist (car p))))
	(group (cdr '(mvpsetq x 2 (y z) (values x (+ x 1)))) 2))

(mapcan #'(lambda (p s) ;; map + concat
	    (shuffle (mklist (car p))
		     s))
	(group (cdr '(mvpsetq x 2 (y z) (values x (+ x 1)))) 2)
	(mapcar #'(lambda (p)
	    (mapcar #'(lambda (x) (gensym))
		    (mklist (car p))))
		(group (cdr '(mvpsetq x 2 (y z) (values x (+ x 1)))) 2)))

(shuffle (mklist (car '(x 1)))
	 (list (gensym)))

(shuffle (mklist (car '((y z) (values 3 4))))
	 (list (gensym) (gensym)))

(defmacro mvdo (binds (test &rest result) &body body)
  (let ((label (gensym))
        (temps (mapcar #'(lambda (b)
                           (if (listp (car b))
                               (mapcar #'(lambda (x)
                                           (gensym))
                                       (car b))
                               (gensym)))
                       binds)))
    `(let ,(mappend #'mklist temps)
       (mvpsetq ,@(mapcan #'(lambda (b var)
                               (list var (cadr b)))
                           binds
                           temps))
       (prog ,(mapcar #'(lambda (b var) (list b var))
                      (mappend #'mklist (mapcar #'car binds))
                      (mappend #'mklist temps))
             ,label
             (if ,test
                 (return (progn ,@result)))
             ,@body
             (mvpsetq ,@(mapcan #'(lambda (b)
                                     (if (third b)
                                         (list (car b)
                                               (third b))))
                                 binds))
             (go ,label)))))

(mappend #'mklist '((a b c) d (e (f g) h) ((i)) j))

(mac (mvdo ((x 1 (1+ x))
	    ((y z) (values 0 0) (values z x))) ;; 更新前のz,xを参照している
	 ((> x 5) (list x y z))
       (print (list x y z))))

;; doの定義にpsetqが必要な点についてはp99で説明した．
;; これをみる
;; => doはそういうものだということ以外に特に言ってない

;; なぜprogに展開するか
;; doでもよさそうだけどあんまり意味がなさそう doが作る束縛は使えないため

;; mvpsetq をみる

;; mvdoをみる

;; 11.6 マクロの必要性

;; ifを関数として書く

(defun fnif (test then &optional else)
  (if test
      (funcall then)
      (if else (funcall else))))

(fnif (rich)
      #'(lambda () (go-sailing))
      #'(lambda () (rob-bank)))


;; マクロでしかできないことはなさそう
;; シンプルにかけるのが本質で思考を用意にしてくれる、
;; みたいなのが最初の方に書いてあった気がする
