
;; rplaca = setcar! in Scheme
;; rplacd = setcdr! in Scheme
;; http://clhs.lisp.se/Body/f_rplaca.htm
(setq x (cons 1 2))
x
(rplaca x 10)
x
(rplacd x 20)
x

;; reverse with side effec
;; do http://www.lispworks.com/documentation/HyperSpec/Body/m_do_do.htm
;; rotatef http://clhs.lisp.se/Body/m_rotate.htm
(defun bad-reverse (lst)
  (let* ((len (length lst))
         (ilimit (truncate (/ len 2)))) ;; truncate = 切り捨て = ceil (普通の整数除算）
    (do ((i 0 (1+ i))
         (j (1- len) (1- j))) ;; varlist :: = (varname init-value how-to-update)*
        ((>= i ilimit)) ;; endlist. 終了条件. (rotatef ..)が実行される前に判定される おそらく複数のcondを置くこともできるのだろう
      (rotatef (nth i lst) (nth j lst)))))

;; NOTE: nth returns store location (pointer)
;; http://clhs.lisp.se/Body/f_nth.htm
(nth 2  (list 1 2 3 4))

(setq lst '(a b c d))
;; step 0:
;; i=0,j=3
;; swap i <-> j
;; (d b c a)
;; step 2:
;; i=1,j=2
;; swap i <-> j
;; (d c b a)
;; step 3:
;; i=2,j=1
;; stop (ilimit = 2, i >= ilmit is true.)
(bad-reverse lst)
lst
(rotatef (nth 0 lst) (nth 1 lst) (nth 2 lst) (nth 3 lst))
lst


;; rotatefマクロは任意の数の汎変数（generalized variable， setfの第1引数として与えることができる式）の値を逆に並び替える
;; 反変数
;; ． 関数的プログラムの構造は全て式内部の引数の構成によって決まり， また引数がインデントされているので，そのインデント方法は多岐に渡る．
;; インデントの話がよくわからない
;; bumpページとは...
(defun good-reverse (lst)
  (labels ((rev (lst acc)
           (if (null lst)
               acc
               (rev (cdr lst) (cons (car lst) acc)))))
    (rev lst nil)))

(setq y '(a b c d))
y
(good-reverse y)
y
;; 「O(n^2)ではなくO(n)なのだ」． bad-reverseもO(n)だが？
(reverse y) ;; 組み込みのreverseはgood-reverse
y

;; 組み込みの破壊的reverse
(nreverse y)
y ;; => (A) ??? どういうことしたらこうなるんだ...

#'nconc
;; nconc http://www.lispworks.com/documentation/HyperSpec/Body/f_nconc.htm
;;  nconc &rest lists => concatenated-list
;; 可変長引数をとり、結合されたリストを返す
;; 以下のように再帰的に定義されている（上のリンクのコピペ）
;; (nconc) =>  ()
;; (nconc nil . lists) ==  (nconc . lists) ;; このときは副作用がない
;; (nconc list) =>  list
;; (nconc list-1 list-2) ==  (progn (rplacd (last list-1) list-2) list-1) list-1のcdrを破壊的にlist2にしている。
;; (nconc list-1 list-2 . lists) ==  (nconc (nconc list-1 list-2) . lists)
(setq x (list 1 2 3 4))
(setq y (list 5 6 7 8))
(nconc x y)
x ;; => (1 2 3 4 5 6 7 8)

y
(setq x nil)
x
(nconc x y)
x ;; xは変更されない。
;; ある関数が破壊的だと書かれていても， その関数が副作用のために呼ばれる筈の関数だということではない
;; このケースだと、nconcは破壊的変更を行うときと行わない場合がある（ひどい）ということを指している
;;

;; 破壊的オペレータに!をつける習慣がないらしいので、判別が難しい...
;; n が破壊的

;; 関数が1個の値しか返せなければ，他の値はパラメータに変更を加えることで「返す」しかない．
;; C言語でswapを書くとき、void swap(int& x, int &y)みたいになるので、そういうことを言っているのだろう


;; 多値
(truncate 26.21875)
(= (truncate 26.21875) 26)

(multiple-value-bind (int frac) (truncate 26.21875)
  (list int frac))

;; 多値返却
(defun powers (x)
    (values x (sqrt x) (expt x 2)))

(multiple-value-bind (base root square) (powers 4)
    (list base root square))

;; 解説っぽいことをちょっと書くけどほとんど調べてない(Schemeの多値と同じだと思って説明してみる)
;; 第一級の値ではないっぽい (!= tuple)
;; (multiple-value-bind (base root square) ...)は以下と同じと考えるといい
;; powersをcpsに書き換える
(defun powers-cps (x k)
  (funcall k x (sqrt x) (expt x 2)))

;; multiple-value-bindを使わずに同じことをやる
(powers-cps 4 (lambda (base root square)
		(list base root square)))
;; これはtupleを作って渡して分解して使うのとは違って
;; tupleを生成しない。効率がいい。
;; 元ネタ -> practical-scheme.net/wiliki/wiliki.cgi?Scheme%3a%c2%bf%c3%cd
;; 第一級継続と多値は関係が深いがCommonLispだと独立に議論されているらしい

(defun fun (x)
  (list 'a (expt (car x) 2)))

(defun imp (x)
  (let (y sqr)
    (setq y (car x))
    (setq sqr (expt y 2))
    (list 'a sqr)))
;; このletの使い方は初めてかも。
;; (var init-form)でなくvarとだけ書くとvarを宣言だけすることができる
;; y, sqrは(let (y sqr) ...)で宣言だけされている(初期化をしないとNILになる)
;; if there is not an init-form associated with a var, var is initialized to nil. 
;; http://www.lispworks.com/documentation/HyperSpec/Body/s_let_l.htm
(let (y)
  y) ;; => NIL

;; 「幸運なことに，命令的プログラムを関数的プログラムに変換するうまい方法がある． この方法は完成したコードに適用することから始めるといい． すぐに勘が働くようになり，コードを書きながら変換できるようになるだろう． そうすればあっという間に， 始めから関数的プログラミングの用語でプログラムを考えるようになるだろう．」
;; 命令形プログラムを関数型プログラムに変換することを考えて、こつを掴んでみよう

;; 逆順に考える
;; impの後の式は(list 'a sqr)
;; sqrをsetqの右側(expt y 2)で置き換える (list 'a (expt y 2))
;; yを(car x)で置き換える (list 'a (expt (car x) 2))

;; いつでも使えるわけではなさそうだがはじめのうちはいいかも？

;; * 関数的インタフェイス

(defun qualify (expr)
  (nconc (copy-list expr) (list 'maybe)))

;; 参考: https://ja.wikipedia.org/wiki/%E5%8F%82%E7%85%A7%E9%80%8F%E9%81%8E%E6%80%A7
;; 「ある式が参照透過であるとは、その式をその式の値に置き換えてもプログラムの振る舞いが変わらない(言い換えれば、同じ入力に対して同じ作用と同じ出力とを持つプログラムになる)ことを言う」

;; qualifymはnconcを呼んでいるが、copyした上で破壊しているから参照透明
;; ただ効率はわるそう

;; それは第1引数として与えられたリストは新たにコンシングされるからだ．
;; consing = copying ???

(let ((x 0))
  (defun total (y)
    (incf x y)))
(setq y 10)
(format nil "~D" 10)
(total y)

;; 「一般的な場合では，オブジェクトをどの関数が支配しているかではなく， どの関数呼び出しが支配しているかについて考えなければいけない．」
;; なんのことか
;; クロージャとかの話なんですかね
;; (f 10)で変化が起きるオブジェクトと,(f 12)で変化が起きるオブジェクトが違うなんてことがあるんだろうか。
;; 内部でdispatchしてれば有り得そうだけど、こういうことを言いたいんだろうか
(let ((x 0)
      (y 0))
  (defun f (tag d)
    (if (eq tag 'x)
	(incf x d)
	(incf y d))))
(f 'x 10)
(f 'y 10)

;; 関係ないけど、topレベルのsetqがダイナミック変数になるっぽいのでとてもつらい...
;; https://g000001.cddddr.org/1232507492
;; define-symbol-macroでやるといい？
;; 助けてくれ...

;; 「関数呼び出しは返り値として受け取るオブジェクトを支配するが， 引数として渡されるオブジェクトは支配しない，というのがLispの慣習のようだ．」
;; " The convention in Lisp seems to bethat an invocation owns objects it receives as return values, but not objects passedto it as arguments."

;; 「関数呼び出しが受け取る」というのが違和感があったけど直訳らしい。it givesじゃないのか...

(setq x '(1 2 3))

(defun ok (x)
  (nconc (list 'a x) (list 'c)))
(ok x)
x
(defun not-ok (x)
  (nconc (list 'a) x (list 'c)))
(not-ok x)
x

;; 「fがvalに対してnconcで何かを連結するのは安全だろうか？ gがidentityのときはそうではない：」
;; xの部分へのポインタを返したらダメ

(defun exclaim (expression)
  (append expression '(oh my)))

(exclaim '(lions and tigers and bears))
;; (nconc * '(goodness))
;; 何かの処理系では*が前の式で評価した結果になるのかもしれない？
;; ならないのでsetqする
(setq ret (exclaim '(lions and tigers and bears)))
ret
(nconc ret '(goodness))
(exclaim '(fixnums and bignums and floats)) ;; 末尾にgoodnessがつく

(defun exclaim (expression)
  (append expression (list 'oh 'my))) ;; 関数呼び出しのたびに新しくリストを生成すべきだ
;; '(oh my)関数呼び出しのたびに生成されないらしい
(let (quoted (list oh my))
  (defun exclaim (expression)
    (append expression quoted))) ;; このように変換されると考えて良さそう
;; Schemeもそうだった気がする...
;; (eq? '(oh my) '(oh my)) => trueならなければならなかったはず(eq?はアドレスの比較)
;; (eq? (list 'oh 'my) (list 'oh 'my)) => false


;; インタラクティブ．プログラミング

;; 「関数が副作用を使うのを避けられないなら， 彼らは少なくともそこに関数的インタフェイスを盛り込もうとする．」
;; 状態をローカルにするってことって理解で結局あってるんだろうか...

;; 「Lispでは，壁自体もうまくデザインすることができる． 」
;; 壁とはなんだろう
