(load "./commons.lisp")

(defpackage generalized-variables
  (:use common-lisp commons))

(in-package generalized-variables)

;; 12.1
(setq lst '(a b c))

(setf (car lst) 480)

lst

;; assertion = ここでは変数代入てきなものを想像すれば良さそう？
;; 副作用をもたらすような何か？
;; 宣言的な何かな気もする
;; web版だとassertionが命令と訳されていて怪しい

;; 最後らへんにもassertionという言葉が出てくるけどそちらは宣言的な何かくらいの意味っぽい？

(defmacro toggle (obj)               ; 間違い
  `(setf ,obj (not ,obj)))

 (let ((lst '(a b c)))
   (toggle (car lst))
   lst)

;; 警告が出る
; caught WARNING:
;   Destructive function SB-KERNEL:%RPLACA called on constant data: (A B C)
;   See also:
;     The ANSI Standard, Special Operator QUOTE
;     The ANSI Standard, Section 3.2.2.3

(defvar *friends* (make-hash-table))

(setf (gethash 'mary *friends*) (make-hash-table))

(setf (gethash 'john (gethash 'mary *friends*)) t)

(gethash 'john (gethash 'mary *friends*))

(defmacro friend-of (p q)
  `(gethash ,p (gethash ,q *friends*)))

(friend-of 'john 'mary)

;; 反射的ではないのか
;;(friend-of 'mary 'john)

(toggle (friend-of 'john 'mary))

(let ((lst '(t nil t))
      (i -1))
  (toggle (nth (incf i) lst))
  lst)

(let ((lst '(t nil t)))
  (setf (nth 0 lst) (not (nth 1 lst)))
  lst)

;; toggleの引数として与えられた式を取り出し， setfの第1引数の場所に挿入するだけでは十分ではない．
;; それが部分式を含むなら，それらが副作用を持つことを考慮し，分割して別個に評価しなければならない
．
;; letで先に評価するのじゃだめなのかな...
;;  一般的に言えば，これは複雑な仕事だ．

;; toggleではかんたんだけど難しいケースもあるということだろうか？

(define-modify-macro toggle () not)

(let ((lst '(t nil t))
        (i -1))
    (toggle (nth (incf i) lst))
    lst)

;; マクロ展開系を比較する

(mac (toggle (nth 0 lst)))

;; xのときはletで１回だけ評価するようにするのがいらないので、
;; 場合分けのコードが必要になる。これを「複雑な仕事」と言っているのかな

(mac (toggle x))

;; 他に考慮すべきことがあるか？

;; 手書きtoggle

;; 書き始めて結構大変だということに気づく
;; nth -> nthcdr + rplaca などの変換をしてやる必要があるので難しい
;; これじゃ全然だめ
(defmacro toggle (obj)
  (if (symbolp obj)
      `(setf ,obj (not ,obj))
      (with-gensyms (obj-sym)
	`(let ((,obj-sym ,obj))
	   (setf ,obj-sym (not ,obj-sym))))))



;;generalized toggle

(defmacro toggle (&rest args)
  `(progn
     ,@(mapcar #'(lambda (a) `(toggle2 ,a))
                args)))

(define-modify-macro toggle2 () not)

(setq x t y t z (list t))

(toggle x y (car z))
(values x y z)

(defmacro allf (val &rest args)
  (with-gensyms (gval)
    `(let ((,gval ,val))
       (setf ,@(mapcan #'(lambda (a) (list a gval))
		       args)))))

(allf 1 x y z)

(values x y z)

(defmacro nilf (&rest args) `(allf nil ,@args))

(defmacro tf (&rest args) `(allf t ,@args))

;;  オペレータを普通の変数のみに対して使うつもりでも， setqでなくsetfに展開されるマクロを書いた方がよいということだ
;; setfの一般性をなんの引替もなく享受できるのだから， マクロ展開ではsetqを使う方が好ましいことはまずないだろう．

(define-modify-macro concf (obj) nconc)

;; nconcについて
;; (nconc x y) と (setq x (nconc x y)) の意味が違うことがある

(let ((x nil)
      (y '(1 2 3)))
  (nconc x y)
  x)

(let ((x nil)
      (y '(1 2 3)))
  (setq x (nconc x y))
  x)

(setq x nil y '(1 2 3))

;; concfが(setq x (nconc x y))のイディオム
(mac (concf x y))

(mac (concf (setf (car x) 1) y))


(define-modify-macro conc1f (obj)
  (lambda (place obj)
    (nconc place (list obj))))

(define-modify-macro concnew (obj &rest args)
  (lambda (place obj &rest args)
    (unless (apply #'member obj place args)
      (nconc place (list obj)))))

;; member
;; http://www.lispworks.com/documentation/HyperSpec/Body/f_mem_m.htm
;;  member item list &key key test test-not => tail

(setq x '(1 2 3))
(mac (conc1f x 4))

(mac (conc1f (car (list x)) 4))

(concnew x 4)

(mac (concnew x 3))
x
;; 今のconcnewだとxがnilになってしまうので良くない気がする。

(define-modify-macro concnew (obj &rest args)
  (lambda (place obj &rest args)
    (if (apply #'member obj place args)
	(nconc place (list obj))
	place)))

;; 12.4 更に複雑なユーティリティ

(setq x 3)

(incf x)

(incf x 10)

;; web番だと、_fがfと表記されているので注意する。

(defmacro _f (op place &rest args)          ; 誤り
  `(setf ,place (,op ,place ,@args)))

;; placeが２回評価されてしまう
;; toggleみたいに色々考えることがある

;; get-setf-methodをつかう

;; aref = array reference
;; http://www.lispworks.com/documentation/HyperSpec/Body/f_aref.htm#aref

(get-setf-method '(aref a (incf i)))
;; 動かない

;; get-setf-expansionをつかう？
;; http://www.lispworks.com/documentation/HyperSpec/Body/f_get_se.htm#get-setf-expansion
;; 名前が変わったのかな

;;  get-setf-expansion place &optional environment
;; => vars, vals, store-vars, writer-form, reader-form

(defmacro show-usage-for-get-setf-expansion (expr)
  (with-gensyms (temp-vars vals store-vars form access)
    `(multiple-value-bind (,temp-vars ,vals ,store-vars ,form ,access) (get-setf-expansion ',expr)
       (format t "vars=~S~%" ,temp-vars)
       (format t "forms=~S~%" ,vals)
       (format t "var=~S~%" ,store-vars)
       (format t "set=~S~%" ,form)
       (format t "access=~S~%" ,access)
       )))

(show-usage-for-get-setf-expansion (aref a (incf i)))

;; 適当に読み替えていく必要がある

;; http://www.lispworks.com/documentation/HyperSpec/Body/05_aab.htm
;; 使い方
;; 本文の定義と対応させている
;; vars: let*でこれに束縛する(複数回の評価や評価の順番がおかしくなるのを避ける)
;; forms: let*temp-varsに束縛させる form
;; var: ユーザーのしたい操作をしたあとのaccess(後述)を入れるための変数.
;; set: let*で作った束縛のもとで評価すべき、placeに対する代入を行う式。値を更新するのに使う。
;; access: 操作前のplaceの値を取る方法

(defmacro _f (op place &rest args)
  (multiple-value-bind (vars forms var set access) (get-setf-expansion place)
    `(let* (,@(mapcar #'list vars forms)
             (,(car var) (,op ,access ,@args)))
       ,set)))

(setq x 1 y 2)

(_f + x y)

;; _f, pull, pull-if, popnはどれも似た形をしているのに注意

(defmacro pull (obj place &rest args)
  (multiple-value-bind (vars forms var set access)
    (get-setf-expansion place)
    (let ((g (gensym)))
      `(let* ((,g ,obj)
              ,@(mapcar #'list vars forms)
              (,(car var) (delete ,g ,access ,@args)))
         ,set))))

(setq x '(1 2 (a b) 3))

(pull 2 x)

;; pullが&rest argsを取っているのはdeleteに渡すオプショナル引数を取るため
(pull '(a b) x :test #'equal)

x

(pushnew 1 x)

(pushnew 2 x)

(setq y '((1 2 (a b) 3)))

(pull 2 (car y))

(pull '(a b) (car y) :test #'equal)

;; define-modify-macroでは第一引数がplaceでなければならない
;; pushnewと合わせたかったのでget-setf-expansionをつかう

(defmacro pull-if (test place &rest args)
  (multiple-value-bind (vars forms var set access)
    (get-setf-expansion place)
    (let ((g (gensym)))
      `(let* ((,g ,test)
              ,@(mapcar #'list vars forms)
              (,(car var) (delete-if ,g ,access ,@args)))
         ,set))))

(let ((lst '(1 2 3 4 5 6)))
  (pull-if #'oddp lst)
  lst)

;;  基盤となる関数がオプショナル引数をとるときは，その上に構築されたマクロもそうなるべき

(defmacro popn (n place)
  (multiple-value-bind (vars forms var set access)
    (get-setf-expansion place)
    (with-gensyms (gn glst)
      `(let* ((,gn ,n)
	      ,@(mapcar #'list vars forms)
	      (,glst ,access)
	      (,(car var) (nthcdr ,gn ,glst)))
	 (prog1 (subseq ,glst 0 ,gn)
	   ,set)))))

(subseq '(a b c d) 1 2)

;; prog1
;; http://www.lispworks.com/documentation/HyperSpec/Body/m_prog1c.htm#prog1

;; prog1 evaluates first-form and then forms, yielding as its only value the primary value yielded by first-form.
;; １つ目の引数が返り値になるよ
;; 評価される順序は書いてあるまま

(setq x '(a b c d e f))

(popn 3 x)

x
(mac (popn 3 x))

;; 任意の(先頭の)部分リストをポップして返す

;; sortf

(defmacro sortf (op &rest places)
  (let* ((meths (mapcar #'(lambda (p)
                            (multiple-value-list
                              (get-setf-expansion p)))
                        places))
         (temps (apply #'append (mapcar #'third meths))))
    `(let* ,(mapcar #'list
                    (mapcan #'(lambda (m)
                                (append (first m)
                                        (third m)))
                            meths)
                    (mapcan #'(lambda (m)
                                (append (second m)
                                        (list (fifth m))))
                            meths))
       ;; gen bubble sort
       ,@(mapcon #'(lambda (rest)
                      (mapcar
                        #'(lambda (arg)
                            `(unless (,op ,(car rest) ,arg)
                               (rotatef ,(car rest) ,arg)))
                        (cdr rest)))
		 temps)
        ;; gen how to update
       ,@(mapcar #'fourth meths)
       )))

(setq x 1 y 2 z 3)

(sortf > x y z)

(list x y z)

;; バブルソートを必要に応じて説明

(mac (sortf > x (aref ar (incf i)) (car lst)))

;; (macがgensymの数値を変更するせいで表示がおかしい気がする) => 多分気の所為？
;; どうもかならずvarに#:NEW1が入ってくるみたいだ. sbclのバグ？
;; 表示が意味がわからなくなる

;; それは少なくともcurlyのケーキ程はあるという規則を立てたいならば，こうすればよい．
;; 「それ」はlarryのケーキ

;; 本だと数式が書いてある

;; マクロのこの部分が生成するコードは引数の数に関して指数的に増大する．
;; O(n^2)を指数的というのを初めて聞いた
;; 誤訳でもなさそう exponentiallyとある

;; _f, sortfは任意のオペレータ（関数に限らず）をとれる

 (let ((x 2))
    (_f nif x 'p 'z 'n)
    x)

;; 12.5

;; defsetf

(defsetf symbol-value set) ;; package lock err

(defsetf car (lst) (new-car)
   `(progn (rplaca ,lst ,new-car)
           ,new-car)) ;; package lock err

;; sbclのsetfはgensymは使わない
(mac (setf (car (progn (print "hoge") (list 1 2))) 3))

;; 非対称なインヴァージョン．

(defvar *cache* (make-hash-table))

(defun retrieve (key)
  (multiple-value-bind (x y) (gethash key *cache*)
    (if y
	(progn
	  (format t "cached.~%")
	  (values x y))
        (cdr (assoc key *world*)))))

;; cacheされたときは多値を返す
;; cacheされないときは値を返す

(defsetf retrieve (key) (val)
  `(setf (gethash ,key *cache*) ,val))

(defvar *world* '((a . 2) (b . 16) (c . 50) (d . 20) (f . 12)))

;; cacheヒットしない
(retrieve 'c)

(setf (retrieve 'n) 77)

;; cacheヒットする
(retrieve 'n)

(multiple-value-bind (x y) (values 1)
  (list x y))

;; p184: bookのp400の脚注に目を通す
