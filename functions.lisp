;; tail recursion

;; for experiment
(defun repeat-n
    (x n)
  (labels ((rec (n acc)
	     (if (= 0 n)
		 acc
		 (rec (- n 1) (cons x acc)))))
    (rec n '())))
(repeat-n 1 10)

;; not tail recursive
(defun our-length (lst)
  (if (null lst)
      0
      (1+ (our-length (cdr lst)))))
(our-length (repeat-n 0 1000)) ;; ok
(our-length (repeat-n 0 10000)) ;; ok
(our-length (repeat-n 0 100000)) ;; control-stack-exhausted-error

;; tail recursive
(defun our-find-if (fn lst)
  (if (funcall fn (car lst))
      (car lst)
      (our-find-if fn (cdr lst))))

;; 「末尾再帰でない関数もしばしば末尾再帰に変換できることがある ．」
;; 本当は常に変換できる。スタックを消費する関数をヒープを消費するように書き換えられる。
;; cps our-length
;; http://www.nct9.ne.jp/m_hiroi/func/abcscm20.html
(defun our-length-cps (lst k)
  (if (null lst)
      (funcall k 0)
      (our-length-cps (cdr lst)
		      (lambda (x)
			(funcall k (+ 1 x))))))
(defun our-length (lst)
  (our-length-cps lst (lambda (x) x)))
(our-length (repeat-n 0 100000)) ;; ok

(defun our-length (lst)
  (labels ((rec (lst acc)
           (if (null lst)
               acc
               (rec (cdr lst) (1+ acc)))))
    (rec lst 0)))
(our-length (list 1 2 3))
(our-length (repeat-n 0 100000))

;; 宣言(optimize speed)は(optimize (speed 3))の 省略形の筈だが，あるCommon Lisp処理系は前者では末尾再帰の最適化をするのに， 後者ではしないことが分かっている． => 知らん
(proclaim '(optimize speed))

;; 末尾再帰形式と型宣言を与えられると， 現在のCommon LispコンパイラはCと同等か，Cより速いコードを生成できる．
;; (triangle n) = 1 + 2 + 3 + 4 + ... + n
(defun triangle (n)
  (labels ((tri (c n)
                (declare (type fixnum n c))
                (if (zerop n) ;; (= 0 n)
                    c ;; accumulator
                    (tri (the fixnum (+ n c)) ;; (+ n c)
                         (the fixnum (- n 1)))))) ;; (- n 1)
    (tri 0 n)))
(triangle 10)
;; 素朴なやつ
(defun triangle-simple (n)
  (if (= n 0)
      0
      (+ n (triangle-simple (- n 1)))))
(triangle-simple 10)

;; What is 'the'?
#'the ;; special form
(macroexpand-1 `(the fixnum (+ n c))) ;; not macro
(the fixnum 1)

;; compilation
(defun foo (x) (1+ x))
(compiled-function-p #'foo) ;; T in SBCL (はじめからcompile済みだった)

(compile 'foo)

(compile nil '(lambda (x) (+ x 2)))
(funcall (compile nil '(lambda (x) (+ x 2))) 1)

;; progn = begin in Scheme = do in Clojure
(compile 'bar '(lambda (x) (* x 3)))
#'bar
(bar 10)

;; ??? vox
(let ((y 2))
  (defun foo (x) (+ x y))) ;; works
(compiled-function-p #'foo)
(compile 'foo) ;; undefined but it works in sbcl
(foo 2)

(compile-file "./functions.lisp")

;; ある関数が別の関数内で作られ，外側の関数がコンパイルされると， 内側の関数もコンパイルされる．
(defun hoge ()
  (labels ((fuga () ;; compile hoge -> compile fuga
	     (+ 1 2)))
    #'fuga))
(compiled-function-p (hoge)) ;; => T

;; bazページとは...?

(defun 50th (lst) (nth 49 lst))

;; inline
(proclaim '(inline 50th))

(defun foo (lst)
  (+ (50th lst) 1)) ;; -> (+ (nth 49 lst) 1) とインライン展開している

;; Lispの初期の方言では，関数がリストとして表現されているものがあった
;; おそらく、構文木をそのまま実行していくようなインタプリタのことを指している？

