(print (package-name *package*))
(defpackage :hoge
  (:use :cl))

(print (package-name *package*))

;;(in-package 'hoge) ;; not works
;; keyword parameter :use not works
(in-package :hoge)

(defun foo ()
  "foo!!!")

(foo)

(package-name *package*)

(symbol-package 'foo)

(in-package :cl)

(package-name *package*)

(hoge::foo)

(in-package :hoge)

(export 'foo)

(in-package :cl)

(hoge:foo)
;; コロン１つで参照できるようになる

(import 'hoge:foo)

(foo)
;; hoge:をつけなくて良くなる

(package-name *package*)
;; 先ほどmine内でfooを評価しようとして失敗したとき， 結果的にシンボルfooをそこでインターンした． fooにはグローバルな値はなく，そのためエラーが出た． しかしインターンは，名前を打ち込んだ結果として行われていた． よって今fooをmineにインポートしようとしたとき， すでに同じ名前のシンボルがあったことになる．
;; よくわからない

(defpackage other
  (:use common-lisp)
  (:export noise))

(in-package :other)

(package-name *package*)

(defun noise (animal)
  (case animal
    (dog 'woof)
    (cat 'meow)
    (pig 'oink)))

(noise 'dog)

(noise 'cat)

(noise 'pig)

(in-package :cl)

(other:noise 'dog)

(other:noise 'other:dog) ;; not works


;; use keyword for data.
;; キーワードは黄金のようなもので，どこでも通用し，それ自身が価値を持つ． つまりどこでも見えるしクォートする必要は一切ない

(in-package :other)

(defun noise (animal)
  (case animal
        (:dog :woof)
        (:cat :meow)
        (:pig :oink)))

(in-package :cl)

(other:noise :dog)
